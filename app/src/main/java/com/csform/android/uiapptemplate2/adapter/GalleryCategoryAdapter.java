package com.csform.android.uiapptemplate2.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.csform.android.uiapptemplate2.R;
import com.squareup.picasso.Picasso;

public class GalleryCategoryAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private String[] imgid;
    private Integer[] category_images_local;
    private final String category;

    public GalleryCategoryAdapter(Activity context, String[] itemname, String[] imgid, String category) {
        super(context, R.layout.left_menu, itemname);

        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
        this.category = category;
    }

    public GalleryCategoryAdapter(Activity context, String[] itemname, Integer[] imgid_local, String category) {
        super(context, R.layout.left_menu, itemname);


        this.context = context;
        this.itemname = itemname;
        this.category_images_local = imgid_local;

        this.category = category;
    }

    public void updateData(String[] imgid) {

        this.imgid = imgid;

    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.grid_category, null, true);


        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);


        if (imgid != null) {
            Picasso.with(getContext())
                    .load(imgid[position])
                    .error(R.drawable.default_image)
                    //      .placeholder(R.drawable.ic_trash)
                    .fit()
                    .into(imageView);
        } else {
            Picasso.with(getContext())
                    .load(category_images_local[position])
                    .error(R.drawable.default_image)
                    //  .placeholder(R.drawable.ic_trash)
                    .fit()
                    .into(imageView);
        }
        return rowView;

    }
}