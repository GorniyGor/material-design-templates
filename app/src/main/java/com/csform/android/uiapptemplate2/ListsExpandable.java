/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.Expandable.ListsExpandableFragment;
import com.csform.android.uiapptemplate2.data.AbstractDataProviderExpandable;
import com.csform.android.uiapptemplate2.fragment.ExpandableDataProviderFragment;

public class ListsExpandable extends BaseActivity {
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_demo, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText("EXPANDABLE");

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(new ExpandableDataProviderFragment(), FRAGMENT_TAG_DATA_PROVIDER)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new ListsExpandableFragment(), FRAGMENT_LIST_VIEW)
                    .commit();
        }
    }

    public AbstractDataProviderExpandable getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((ExpandableDataProviderFragment) fragment).getDataProvider();
    }
}
