package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.fragment.CardProfileFragment;


public class CardsOverHeader extends BaseActivityCardsOverHeader {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar_collapsing);
        //final CollapsingToolbarLayout mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.main_collapsing);
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame_2);
        getLayoutInflater().inflate(R.layout.view_red, contentFrameLayout);
        final FragmentTransaction fr = getSupportFragmentManager().beginTransaction();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fr.replace(R.id.content_frame_red, new CardProfileFragment());
                fr.commit();
            }
        }, 100);

        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.parallax_header_title);


        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (verticalOffset < 0) {
                    FrameLayout overlay = (FrameLayout) findViewById(R.id.overlay_frame);
                    overlay.setVisibility(View.VISIBLE);
                    mToolbar.setElevation(8);
                } else {

                    FrameLayout overlay = (FrameLayout) findViewById(R.id.overlay_frame);
                    overlay.setVisibility(View.GONE);
                    mToolbar.setElevation(0);
                }

            }
        });
        //setSupportActionBar(toolbar);

      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        //mToolbar.inflateMenu(R.menu.menu_main);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_scrolling, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}


