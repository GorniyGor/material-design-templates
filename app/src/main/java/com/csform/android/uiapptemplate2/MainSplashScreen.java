package com.csform.android.uiapptemplate2;


import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.csform.android.uiapptemplate2.kbv.KenBurnsView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;


public class MainSplashScreen extends AppCompatActivity {
    int SPLASH_TIME_OUT = 3000;


    //1- fetch data from Firebase database and images from Firebase storage
    public static final int FIREBASE_ENABLED = 0;

    private KenBurnsView mKenBurns;
    private TextView mLogo;
    private TextView welcomeText;
    private boolean notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar


        //open web page from notification
        if (getIntent().getExtras() != null) {

            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                if (key.equals("link")) {
                    notification = true;

                   /*Intent i = new Intent(MainSplashScreen.this, MainActivity.class);
                    i.putExtra("nowizzard", "1");
                    startActivity(i);*/
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(value.toString()));
                    startActivity(browserIntent);



                }
               /* else
                {
                    Intent i = new Intent(MainSplashScreen.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }*/
            }
        }
        if (!notification) {
         /* storing subscribed status, cheching if  user is registered on mailchimp list, if not starting count on views, on number of
          SUBSCRIBE_VIEW showing subscribe dialog*/
            SharedPreferences firebaseActive = getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
            SharedPreferences.Editor ed = firebaseActive.edit();
            ed.putInt("firebase_enabled", FIREBASE_ENABLED);
            ed.apply();


            if (!preferenceFileExist("SUBSCRIBE")) {
                SharedPreferences subscribe = getApplicationContext().getSharedPreferences("SUBSCRIBE", MODE_PRIVATE);
                SharedPreferences.Editor editor = subscribe.edit();
                editor.putInt("subscribed", 0);
                editor.putInt("numOfViews", 1);

                editor.putInt("canceled", 0);
                editor.apply();
                DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference();

                ref1.addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        Intent i = new Intent(MainSplashScreen.this, MainActivity.class);
                        i.putExtra("wizzard", "1");
                        startActivity(i);

                        // close this activity
                        finish();

                    }
                }, SPLASH_TIME_OUT);


            } else {
                SharedPreferences subscribe = getApplicationContext().getSharedPreferences("SUBSCRIBE", MODE_PRIVATE);
                int subscribeStatus = subscribe.getInt("subscribed", 0);
                if (subscribeStatus == 0) {
                    SharedPreferences.Editor editor = subscribe.edit();
                    editor.putInt("numOfViews", 1);
                    editor.apply();
                }
                new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        Intent i = new Intent(MainSplashScreen.this, MainActivity.class);

                        startActivity(i);

                        // close this activity
                        finish();
                    }
                }, SPLASH_TIME_OUT);
            }


            setContentView(R.layout.activity_splash_screen);

            mKenBurns = (KenBurnsView) findViewById(R.id.ken_burns_images);
            mLogo = (TextView) findViewById(R.id.logo);
            welcomeText = (TextView) findViewById(R.id.welcome_text);
            setAnimation();


        } else {
            finish();
        }

    }

    public boolean preferenceFileExist(String fileName) {
        File f = new File(getApplicationContext().getApplicationInfo().dataDir + "/shared_prefs/"
                + fileName + ".xml");
        return f.exists();
    }

    /**
     * Animation depends on category.
     */
    private void setAnimation() {

            mKenBurns.setImageResource(R.drawable.splash2_bcg);
            animation2();
            animation3();

    }



    private void animation2() {
        mLogo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate_top_to_center);
        mLogo.startAnimation(anim);
    }

    private void animation3() {
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(welcomeText, "alpha", 0.0F, 1.0F);
        alphaAnimation.setStartDelay(1700);
        alphaAnimation.setDuration(500);
        alphaAnimation.start();
    }


}
