/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.Swipeable;

import android.content.SharedPreferences;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.csform.android.uiapptemplate2.ListSwipeableWithButton;
import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.AbstractDataProviderSwipeable;
import com.csform.android.uiapptemplate2.utils.HidingScrollListener;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.SwipeDismissItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.touchguard.RecyclerViewTouchActionGuardManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;

public class ListsSwipeableWithButtonFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewSwipeManager mRecyclerViewSwipeManager;
    private RecyclerViewTouchActionGuardManager mRecyclerViewTouchActionGuardManager;
    private ListsSwipeableWithButtonAdapter myItemAdapter;
    public AbstractDataProviderSwipeable mProvider;

    public ListsSwipeableWithButtonFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        SharedPreferences subscribe = getActivity().getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);

        //noinspection ConstantConditions
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());

        // touch guard manager  (this class is required to suppress scrolling while swipe-dismiss animation is running)
        mRecyclerViewTouchActionGuardManager = new RecyclerViewTouchActionGuardManager();
        mRecyclerViewTouchActionGuardManager.setInterceptVerticalScrollingWhileAnimationRunning(true);
        mRecyclerViewTouchActionGuardManager.setEnabled(true);

        // swipe manager
        mRecyclerViewSwipeManager = new RecyclerViewSwipeManager();

        //adapter
        myItemAdapter = new ListsSwipeableWithButtonAdapter(getDataProvider());
        myItemAdapter.setEventListener(new ListsSwipeableWithButtonAdapter.EventListener() {
            @Override
            public void onItemPinned(int position) {

                final int pos = position;

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((ListSwipeableWithButton) getActivity()).onItemPinned(pos);
                    }
                }, 100);

                //  ((ListSwipeableWithButton) getActivity()).onItemPinned(position);

            }

            @Override
            public void onItemViewClicked(View v) {

                handleOnItemViewClicked(v);
            }

            @Override
            public void onUnderSwipeableViewButtonClicked(View v) {

                handleOnUnderSwipeableViewButtonClicked(v);
            }

            @Override
            public void onUnderSwipeableViewButtonClicked1(View v) {

                handleOnUnderSwipeableViewButtonClicked1(v);
            }
        });

        mAdapter = myItemAdapter;

        mWrappedAdapter = mRecyclerViewSwipeManager.createWrappedAdapter(myItemAdapter);      // wrap for swiping

        final GeneralItemAnimator animator = new SwipeDismissItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Disable the change animation in order to make turning back animation of swiped item works properly.
        animator.setSupportsChangeAnimations(false);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        mRecyclerView.setItemAnimator(animator);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        if (firebaseStatus == 1) {
            /*final int[] tries = new int[1];
            tries[0]=0;
            final Handler handler = new Handler(getContext().getMainLooper());
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        if(myItemAdapter.getFirebase_done()==1) {
                            mAdapter.notifyDataSetChanged();
                            mWrappedAdapter.notifyDataSetChanged();
                        }
                        while(myItemAdapter.getFirebase_done()!=1 && tries[0]<4) {
                            sleep(500);
                            tries[0]=tries[0]+1;
                            handler.post(this);
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            thread.start();*/
            final Timer timerObj = new Timer();
            timerObj.schedule(new TimerTask() {
                int count = 1;

                @Override
                public void run() {
                    count++;
                    // Your logic here...

                    // When you need to modify a UI element, do so on the UI thread.
                    // 'getActivity()' is required as this is being ran from a Fragment.
                    try {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                // This code will always run on the UI thread, therefore is safe to modify UI elements.
                                if (myItemAdapter.getFirebase_done() == 1) {
                                    mAdapter.notifyDataSetChanged();
                                    mWrappedAdapter.notifyDataSetChanged();

                                    timerObj.cancel();
                                    timerObj.purge();
                                }
                                if (count == 100) {
                                    timerObj.cancel();
                                    timerObj.purge();
                                }
                            }
                        });
                    } catch (Exception e) {
                        timerObj.cancel();
                        timerObj.purge();

                    }
                }
            }, 0, 100);


        }

        // additional decorations
        //noinspection StatementWithEmptyBody
        if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z1)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable.list_divider_h), true));

        // NOTE:
        // The initialization order is very important! This order determines the priority of touch event handling.
        //
        // priority: TouchActionGuard > Swipe > DragAndDrop
        mRecyclerViewTouchActionGuardManager.attachRecyclerView(mRecyclerView);
        mRecyclerViewSwipeManager.attachRecyclerView(mRecyclerView);

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(R.id
                .coordinatorLayout);
        final FloatingActionButton fam = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fam.setVisibility(View.VISIBLE);
        fam.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "FAB Button pressed", Snackbar.LENGTH_LONG);

                snackbar.show();
            }
        });
        final Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        mRecyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fam.getLayoutParams();
                int fabBottomMargin = lp.bottomMargin;
                fam.animate().translationY(fam.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }

            @Override
            public void onShow() {

                toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                fam.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }
        });


        // for debugging
//        animator.setDebug(true);
//        animator.setMoveDuration(2000);
//        mRecyclerViewSwipeManager.setMoveToOutsideWindowAnimationDuration(2000);
//        mRecyclerViewSwipeManager.setReturnToDefaultPositionAnimationDuration(2000);
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewSwipeManager != null) {
            mRecyclerViewSwipeManager.release();
            mRecyclerViewSwipeManager = null;
        }

        if (mRecyclerViewTouchActionGuardManager != null) {
            mRecyclerViewTouchActionGuardManager.release();
            mRecyclerViewTouchActionGuardManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mAdapter = null;
        mLayoutManager = null;

        super.onDestroyView();
    }

    private void handleOnItemViewClicked(View v) {
        int position = mRecyclerView.getChildAdapterPosition(v);
        if (position != RecyclerView.NO_POSITION) {
            ((ListSwipeableWithButton) getActivity()).onItemClicked(position);
        }
    }

    private void handleOnUnderSwipeableViewButtonClicked(View v) {
        int position = mRecyclerView.getChildAdapterPosition(v);


        myItemAdapter.mProvider.removeItem(position);
        mAdapter.notifyItemRemoved(position);

        notifyItemChanged(position);


        if (position != RecyclerView.NO_POSITION) {

            ((ListSwipeableWithButton) getActivity()).onItemButtonClicked(position);
        }
    }

    private void handleOnUnderSwipeableViewButtonClicked1(View v) {
        int position = mRecyclerView.getChildAdapterPosition(v);

        if (position != RecyclerView.NO_POSITION) {

            ((ListSwipeableWithButton) getActivity()).onItemButtonClicked1(position);
        }
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    public AbstractDataProviderSwipeable getDataProvider() {
        return ((ListSwipeableWithButton) getActivity()).getDataProvider();
    }

    public void notifyItemChanged(final int position) {
     /*  Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!mRecyclerView.isComputingLayout()) {
                  mAdapter.notifyItemChanged(position);
                } else {
                    notifyItemChanged(position);
                }
            }
        });*/

        mAdapter.notifyItemChanged(position);

    }

    public void notifyItemInserted(int position) {
        mAdapter.notifyItemInserted(position);
        mRecyclerView.scrollToPosition(position);
    }

    public void notifyItemRemoved(int position) {
        myItemAdapter.notifyItemRemoved(position);
        // mRecyclerView.setAdapter(null);
        mWrappedAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRemoved(position);


        mRecyclerView.setAdapter(mWrappedAdapter);

    }
}
