package com.csform.android.uiapptemplate2;


public enum TabObject {

    PROFILE(R.string.red, R.layout.view_green),
    FOLLOW(R.string.blue, R.layout.view_red),
    RATE(R.string.green, R.layout.view_blue),
    POSTWITHIMAGE(R.string.red1, R.layout.view_green_1),
    PROFILE1(R.string.blue1, R.layout.view_red_1),
    FOLLOW2(R.string.green1, R.layout.view_blue_1);


    private int mTitleResId;
    private int mLayoutResId;

    TabObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
