package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.fragment.ListItemDetailsFragment;


public class ListItemDetails extends BaseActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);

        toolbartitle.setText(R.string.lists_item_details_title);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, new ListItemDetailsFragment());
        ft.commit();






    }

}

