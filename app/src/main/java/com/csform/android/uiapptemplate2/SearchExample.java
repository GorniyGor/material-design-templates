package com.csform.android.uiapptemplate2;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.adapter.ListItemDetailsAdapter;
import com.csform.android.uiapptemplate2.data.ListItemDetailsDataProvider;

import java.util.ArrayList;
import java.util.List;



public class SearchExample extends BaseActivity {
    AppBarLayout appbar;
    private List<ListItemDetailsDataProvider> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ListItemDetailsAdapter mAdapter;
    private ListItemDetailsAdapter mAdapterSearch;
    private Context context;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.search_example, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.search_title);
        final TextView numresult = (TextView) findViewById(R.id.textResults);
        numresult.setVisibility(View.GONE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.getBackground().setAlpha(0);
        // toolbar.setElevation(0);
        //drawerLayout.setAlpha(1);
        appbar = (AppBarLayout) findViewById(R.id.app_bar);

        // appbar.setElevation(0);
        appbar.setOutlineProvider(null);
        ConstraintLayout searchContainer = (ConstraintLayout) findViewById(R.id.container_Search);
        searchContainer.setElevation(8);

        appbar.getBackground().setAlpha(0);
        EditText search = (EditText) findViewById(R.id.textSearch);
        search.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                List<ListItemDetailsDataProvider> itemListSearch = new ArrayList<>();


                for (int i = 0; i < itemList.size(); i++) {

                    ListItemDetailsDataProvider movie = itemList.get(i);

                    if (movie.getItem_title().toUpperCase().contains(s.toString()) || movie.getItem_subtitle().toUpperCase().contains(s.toString())) {
                        itemListSearch.add(movie);
                    }
                }
                if (s.length() < 1) {
                    recyclerView.swapAdapter(mAdapter, false);
                    numresult.setVisibility(View.GONE);
                } else {
                    mAdapterSearch = new ListItemDetailsAdapter(itemListSearch);
                    recyclerView.swapAdapter(mAdapterSearch, false);
                    numresult.setVisibility(View.VISIBLE);
                    numresult.setText(itemListSearch.size() + " Results");
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });
        this.context = getApplicationContext();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_search);
       /* recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Intent intent = new Intent(SearchExample.this, ListItemDetailsSingle.class);
                        intent.putExtra("position",position);
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );*/

        mAdapter = new ListItemDetailsAdapter(itemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        prepareSearchData();

        ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {
            //and in your imlpementaion of
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
               /* // get the viewHolder's and target's positions in your adapter data, swap them
                Collections.swap(itemList, viewHolder.getAdapterPosition(), target.getAdapterPosition());
                // and notify the adapter that its dataset has changed
                mAdapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;*/
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                itemList.remove(viewHolder.getAdapterPosition());
                recyclerView.getAdapter().notifyItemRemoved(viewHolder.getAdapterPosition());
            }

            //defines the enabled move directions in each state (idle, swiping, dragging).
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                //return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                //        ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(0, 0);

            }
        };
        ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
        ith.attachToRecyclerView(recyclerView);

    }

    private void prepareSearchData() {
        ListItemDetailsDataProvider item = new ListItemDetailsDataProvider("Grant Marshall", "Warsaw", R.drawable.ic_avatarcaptain, "7186", "Photos", "40", "Followers", "58", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Pena Valdez", "Dunnavant", R.drawable.ic_avatarshowman, "2281", "Photos", "58", "Followers", "16", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Jessica Miles", "Ryderwood", R.drawable.ic_avatardisc_jockey, "1748", "Photos", "55", "Followers", "92", "Following");
        itemList.add(item);
        item = new ListItemDetailsDataProvider("Kerri Barber", "Enlow", R.drawable.ic_avatarastronaut, "3450", "Photos", "41", "Followers", "93", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Natasha Gamble", "Ferney", R.drawable.ic_avatardetective, "7374", "Photos", "48", "Followers", "43", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("White Castaneda", "Lithium", R.drawable.ic_avatardisc_jockey, "6070", "Photos", "70", "Followers", "62", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Vanessa Ryan", "Como", R.drawable.ic_avatardiver, "8158", "Photos", "6", "Followers", "81", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Meredith Hendricks", "Carrizo", R.drawable.ic_avatardetective, "292", "Photos", "85", "Followers", "2", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Carol Kelly", "Brewster", R.drawable.ic_avatardiver, "9231", "Photos", "59", "Followers", "4", "Following");
        itemList.add(item);

        item = new ListItemDetailsDataProvider("Barrera Ramsey", "Fowlerville", R.drawable.ic_avatarshowman, "9972", "Photos", "24", "Followers", "51", "Following");
        itemList.add(item);


        mAdapter.notifyDataSetChanged();
    }
}
