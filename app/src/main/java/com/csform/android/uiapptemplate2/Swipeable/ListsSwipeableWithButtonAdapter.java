/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.Swipeable;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.AbstractDataProviderSwipeable;
import com.csform.android.uiapptemplate2.utils.ViewUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDefault;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionMoveToSwipedDirection;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import com.squareup.picasso.Picasso;

class ListsSwipeableWithButtonAdapter
        extends RecyclerView.Adapter<ListsSwipeableWithButtonAdapter.MyViewHolder>
        implements SwipeableItemAdapter<ListsSwipeableWithButtonAdapter.MyViewHolder> {
    private static final String TAG = "MySwipeableItemAdapter";

    // NOTE: Make accessible with short card_title
    private interface Swipeable extends SwipeableItemConstants {
    }

    public AbstractDataProviderSwipeable mProvider;
    private EventListener mEventListener;
    private View.OnClickListener mSwipeableViewContainerOnClickListener;
    private View.OnClickListener mUnderSwipeableViewButtonOnClickListener;
    private View.OnClickListener mUnderSwipeableViewButtonOnClickListener1;

    public interface EventListener {
        void onItemPinned(int position);

        void onItemViewClicked(View v);

        void onUnderSwipeableViewButtonClicked(View v);

        void onUnderSwipeableViewButtonClicked1(View v);
    }

    public static class MyViewHolder extends AbstractSwipeableItemViewHolder {
        public FrameLayout mContainer;
        public RelativeLayout mBehindViews;
        public TextView mTextViewYear;
        public TextView mTextViewName;
        public TextView mTextViewEmail;
        //public Button mButton;
        public ImageView mButton;
        public ImageView mButton2;
        public ImageView mAvatar;

        public MyViewHolder(View v) {
            super(v);
            mContainer = (FrameLayout) v.findViewById(R.id.container);
            mBehindViews = (RelativeLayout) v.findViewById(R.id.behind_views);
            mTextViewYear = (TextView) v.findViewById(R.id.textYear);
            mTextViewName = (TextView) v.findViewById(R.id.textName);
            mTextViewEmail = (TextView) v.findViewById(R.id.textEmail);
            //  mButton = (Button) v.findViewById(android.R.id.button1);
            mButton = (ImageView) v.findViewById(android.R.id.button1);
            mButton2 = (ImageView) v.findViewById(android.R.id.button2);
            mAvatar = (ImageView) v.findViewById(R.id.imageAvatar);
        }

        @Override
        public View getSwipeableContainerView() {
            return mContainer;
        }

    }

    public ListsSwipeableWithButtonAdapter(AbstractDataProviderSwipeable dataProvider) {
        mProvider = dataProvider;
        mSwipeableViewContainerOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSwipeableViewContainerClick(v);
            }
        };
        mUnderSwipeableViewButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUnderSwipeableViewButtonClick(v);
            }
        };
        mUnderSwipeableViewButtonOnClickListener1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUnderSwipeableViewButtonClick1(v);
            }
        };

        // SwipeableItemAdapter requires stable ID, and also
        // have to implement the getItemId() method appropriately.
        setHasStableIds(true);
    }

    private void onSwipeableViewContainerClick(View v) {
        if (mEventListener != null) {
            mEventListener.onItemViewClicked(
                    RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
        }
    }

    private void onUnderSwipeableViewButtonClick(View v) {
        if (mEventListener != null) {
            mEventListener.onUnderSwipeableViewButtonClicked(
                    RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
        }
        //mProvider.removeItem(getAdapterPosition());
        //  AbstractDataProviderSwipeable.Data item = mAdapter.mProvider.remove(mPosition);

    }

    private void onUnderSwipeableViewButtonClick1(View v) {
        if (mEventListener != null) {
            mEventListener.onUnderSwipeableViewButtonClicked1(
                    RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
        }
    }

    @Override
    public long getItemId(int position) {
        return mProvider.getItem(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return mProvider.getItem(position).getViewType();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_with_leave_behind_button, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final AbstractDataProviderSwipeable.Data item = mProvider.getItem(position);
        StorageReference mStorageRef;

        // set listeners
        // (if the item is *pinned*, click event comes to the mContainer)
        holder.mContainer.setOnClickListener(mSwipeableViewContainerOnClickListener);
        holder.mButton.setOnClickListener(mUnderSwipeableViewButtonOnClickListener1);
        holder.mButton2.setOnClickListener(mUnderSwipeableViewButtonOnClickListener);

        // set text

        holder.mTextViewYear.setText(item.getTextYear());
        holder.mTextViewName.setText(item.getTextName());
        holder.mTextViewEmail.setText(item.getTextEmail());


        // holder.mAvatar.setImageResource(item.getPic_resource());


        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);

        if (item.getPic_resource() != null) {


            holder.mAvatar.setImageResource(item.getPic_resource());
        } else {
            if (item.getAvatar_image_url() == null || item.getAvatar_image_url().equals("")) {


                if (item.getPicture_link() != null) {


                    if (!item.getPicture_link().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("lists/swipe/" + item.getPicture_link()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.mAvatar.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.mAvatar);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.mAvatar.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {


                        holder.mAvatar.setImageDrawable(transparentDrawable);
                    }

                } else {

                    holder.mAvatar.setImageDrawable(transparentDrawable);
                }

            } else {
                Picasso
                        .with(holder.mAvatar.getContext())
                        .load(item.getAvatar_image_url())
                        .fit() // will explain later
                        .into(holder.mAvatar);

            }


        }


        // set background resource (target view ID: container)
        //final int swipeState = holder.getSwipeStateFlags();

        /*if ((swipeState & Swipeable.STATE_FLAG_IS_UPDATED) != 0) {
            int bgResId;

            if ((swipeState & Swipeable.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.bg_item_swiping_active_state;
            } else if ((swipeState & Swipeable.STATE_FLAG_SWIPING) != 0) {
                bgResId = R.drawable.bg_item_swiping_state;
            } else {
                bgResId = R.drawable.bg_item_normal_state;
            }

            //  holder.mContainer.setBackgroundResource(bgResId);
        }*/


        // Or, it can be specified in pixels instead of proportional value.
        float density = holder.itemView.getResources().getDisplayMetrics().density;
        float pinnedDistance = (density * 145); // 100 dp

        holder.setProportionalSwipeAmountModeEnabled(false);
        holder.setMaxLeftSwipeAmount(-pinnedDistance);
        holder.setMaxRightSwipeAmount(0);
        holder.setSwipeItemHorizontalSlideAmount(item.isPinned() ? -pinnedDistance : 0);
    }

    @Override
    public int getItemCount() {
        return mProvider.getCount();
    }

    public int getFirebase_done() {
        return mProvider.getFirebase_loaded();
    }


    @Override
    public int onGetSwipeReactionType(MyViewHolder holder, int position, int x, int y) {
        if (ViewUtils.hitTest(holder.getSwipeableContainerView(), x, y)) {
            return Swipeable.REACTION_CAN_SWIPE_BOTH_H;
        } else {
            return Swipeable.REACTION_CAN_NOT_SWIPE_BOTH_H;
        }
    }

    @Override
    public void onSetSwipeBackground(MyViewHolder holder, int position, int type) {
        if (type == Swipeable.DRAWABLE_SWIPE_NEUTRAL_BACKGROUND) {
            holder.mBehindViews.setVisibility(View.GONE);
        } else {
            holder.mBehindViews.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public SwipeResultAction onSwipeItem(MyViewHolder holder, int position, int result) {


        switch (result) {
            // swipe left --- pin
            case Swipeable.RESULT_SWIPED_LEFT:
                return new SwipeLeftResultAction(this, position);
            // other --- do nothing
            case Swipeable.RESULT_SWIPED_RIGHT:
            case Swipeable.RESULT_CANCELED:
            default:
                if (position != RecyclerView.NO_POSITION) {
                    return new UnpinResultAction(this, position);
                } else {
                    return null;
                }
        }
    }

    public EventListener getEventListener() {
        return mEventListener;
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    private static class SwipeLeftResultAction extends SwipeResultActionMoveToSwipedDirection {
        private ListsSwipeableWithButtonAdapter mAdapter;
        private final int mPosition;
        private boolean mSetPinned;

        SwipeLeftResultAction(ListsSwipeableWithButtonAdapter adapter, int position) {
            mAdapter = adapter;
            mPosition = position;
        }

        @Override
        protected void onPerformAction() {
            super.onPerformAction();

            AbstractDataProviderSwipeable.Data item = mAdapter.mProvider.getItem(mPosition);

            if (!item.isPinned()) {
                item.setPinned(true);
                mAdapter.notifyItemChanged(mPosition);
                mSetPinned = true;
            }
        }

        @Override
        protected void onSlideAnimationEnd() {
            super.onSlideAnimationEnd();

            if (mSetPinned && mAdapter.mEventListener != null) {
                mAdapter.mEventListener.onItemPinned(mPosition);
            }
        }

        @Override
        protected void onCleanUp() {
            super.onCleanUp();
            // clear the references
            mAdapter = null;
        }
    }

    private static class UnpinResultAction extends SwipeResultActionDefault {
        private ListsSwipeableWithButtonAdapter mAdapter;
        private final int mPosition;

        UnpinResultAction(ListsSwipeableWithButtonAdapter adapter, int position) {
            mAdapter = adapter;
            mPosition = position;
        }

        @Override
        protected void onPerformAction() {
            super.onPerformAction();

            AbstractDataProviderSwipeable.Data item = mAdapter.mProvider.getItem(mPosition);
            if (item.isPinned()) {
                item.setPinned(false);
                mAdapter.notifyItemChanged(mPosition);
            }
        }

        @Override
        protected void onCleanUp() {
            super.onCleanUp();
            // clear the references
            mAdapter = null;
        }
    }
}
