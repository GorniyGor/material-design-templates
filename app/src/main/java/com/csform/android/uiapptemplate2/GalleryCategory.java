package com.csform.android.uiapptemplate2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.adapter.GalleryCategoryAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



/*public class GalleryCategory extends BaseActivity {*/
public class GalleryCategory extends BaseActivity {
    public static final String EXTRA_MESSAGE = "IMAGE_URL";
    public static final String EXTRA_CATEGORY = "CATEGORY";
    private String category;
    String[] imgUrl = {
            "https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/115045/pexels-photo-115045.jpeg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/2324/skyline-buildings-new-york-skyscrapers.jpg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/67843/splashing-splash-aqua-water-67843.jpeg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/50582/selfie-monkey-self-portrait-macaca-nigra-50582.jpeg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/32267/pexels-photo.jpg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/7138/summer-rocks-trees-river.jpg?w=940&h=650&auto=compress&cs=tinysrgb",
            "https://images.pexels.com/photos/156731/pexels-photo-156731.jpeg?w=940&h=650&auto=compress&cs=tinysrgb"


    };
    Integer[] img_local;
    String[] imgUrl_names = {


    };
    String[] itemname = {
            "ANIMALS",
            "MOUNTAINS",
            "CITIES",
            "WATER",
            "MONKEY",
            "EYES",
            "RIVERS",
            "LAUGH"


    };
    Integer[] category_images =
            {
                    R.drawable.abstract_picture,
                    R.drawable.bird,
                    R.drawable.aiplane,
                    R.drawable.abstract_red,
                    R.drawable.branch_hand,
                    R.drawable.bridge,
                    R.drawable.building,
                    R.drawable.cave,
                    R.drawable.fireworks,
                    R.drawable.fruits,
                    R.drawable.jellyfish,
                    R.drawable.landscape,
                    R.drawable.man_sea,
                    R.drawable.man_space,
                    R.drawable.minimal_building,
                    R.drawable.mountains,
                    R.drawable.music_concert,
                    R.drawable.music_dj,
                    R.drawable.nature,
                    R.drawable.orange_jellyfish,
                    R.drawable.our_of_space,
                    R.drawable.photographer

            };
    Integer[] animals_local = category_images;
    Integer[] mountains_local = category_images;
    Integer[] cities_local = category_images;
    Integer[] water_local = category_images;
    Integer[] monkey_local = category_images;
    Integer[] eyes_local = category_images;
    Integer[] rivers_local = category_images;
    Integer[] laugh_local = category_images;


    GalleryCategoryAdapter adapter;
    GridView gridView;

    private StorageReference mStorageRef;
    int count = 0;
    List<String> listImgUrl = new ArrayList<String>();
    final ArrayList<PictureCategory> theCategoryList = new ArrayList<>();

    public static class PictureCategory {

        public String image_name;
        public String category;

        public PictureCategory() {

        }

        public PictureCategory(String name, String category) {
            this.image_name = name;
            this.category = category;

        }

        public String getName() {
            return image_name;
        }

        public String getCategory() {
            return category;
        }
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.gallery_category, contentFrameLayout);

        Intent intent = getIntent();
        category = intent.getStringExtra(GalleryCategories.EXTRA_MESSAGE);
        if (category == null) category = "CATEGORY";


        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(category);

        SharedPreferences subscribe = getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);


        if (firebaseStatus == 0) {
            Integer[] category_photos_local;


            switch (category) {

                case "ANIMALS":
                    category_photos_local = animals_local;
                    break;
                case "MOUNTAINS":
                    category_photos_local = mountains_local;
                    break;
                case "CITIES":
                    category_photos_local = cities_local;
                    break;
                case "WATER":
                    category_photos_local = water_local;
                    break;
                case "MONKEY":
                    category_photos_local = monkey_local;
                    break;
                case "EYES":
                    category_photos_local = eyes_local;
                    break;
                case "RIVERS":
                    category_photos_local = rivers_local;
                    break;
                case "LAUGH":
                    category_photos_local = laugh_local;
                    break;
                default:
                    category_photos_local = category_images;
                    break;
            }
            img_local = category_photos_local;

            String[] testobject = new String[category_photos_local.length - 1];

            adapter = new GalleryCategoryAdapter(this, testobject, category_photos_local, category);
            gridView = (GridView) contentFrameLayout.findViewById(R.id.gridViewSingleCategory);
            gridView.setAdapter(adapter);


            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    selectPicture(position);


                                                }
                                            }

            );

        } else {
            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
            DatabaseReference ref1 = database1.getReference("Gallery");

            ref1.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                    PictureCategory newPost = dataSnapshot.getValue(PictureCategory.class);

                    if (newPost.category.equals(category)) {

                        theCategoryList.add(newPost);

                        count++;
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        getFirebasePopulatedData();
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        }


    }

    @Override
    public void onBackPressed() {

        if (!menuclicked) {

            Intent intent = new Intent(this, GalleryCategories.class);

            startActivity(intent);
        } else
            super.onBackPressed();
    }

    private void getFirebasePopulatedData() {


        imgUrl_names = new String[count];
        for (int i = 0; i < count; i++) {
            imgUrl_names[i] = theCategoryList.get(i).getName();

        }


        imgUrl = new String[count];
        for (int i = 0; i < count; i++) {
            final int counter = i;

            imgUrl[i] = theCategoryList.get(i).getName();

            mStorageRef.child("gallery/" + category + "/" + theCategoryList.get(i).getName()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    adapter.updateData(imgUrl);
                    adapter.notifyDataSetChanged();

                    String uristring = uri.toString();
                    imgUrl[counter] = uristring;

                }

            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {


                }
            });

        }
        adapter = new GalleryCategoryAdapter(this, imgUrl, imgUrl, category);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        gridView = (GridView) contentFrameLayout.findViewById(R.id.gridViewSingleCategory);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                selectPicture(position);


                                            }
                                        }
        );


    }

    private void selectPicture(int position) {
        Intent intent = new Intent(this, GalleryFullScreen.class);
        String first_image_to_show;

        SharedPreferences subscribe = getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);


        if (firebaseStatus == 0) {
            first_image_to_show = Integer.toString(img_local[position]);
            ArrayList<Integer> intList = new ArrayList<Integer>(Arrays.asList(img_local));
            intent.putIntegerArrayListExtra("pictures_local", intList);
            int numofimages = img_local.length;
            imgUrl_names = new String[numofimages];
            for (int i = 0; i < numofimages; i++) {
                imgUrl_names[i] = getResources().getResourceEntryName(img_local[i]);

            }
            ArrayList<String> stringList1 = new ArrayList<String>(Arrays.asList(imgUrl_names));
            intent.putStringArrayListExtra("pictures_names", stringList1);

        } else {
            first_image_to_show = imgUrl[position];
            ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(imgUrl));
            intent.putStringArrayListExtra("pictures_url", stringList);
            intent.putStringArrayListExtra("pictures_names", stringList);
        }

        intent.putExtra(EXTRA_MESSAGE, first_image_to_show);
        intent.putExtra("position", Integer.toString(position));

        intent.putExtra(EXTRA_CATEGORY, category);

        startActivity(intent);

    }

}


