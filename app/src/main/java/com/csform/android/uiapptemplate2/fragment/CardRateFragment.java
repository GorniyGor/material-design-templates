package com.csform.android.uiapptemplate2.fragment;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.adapter.CardsRateAdapter;
import com.csform.android.uiapptemplate2.data.CardsRateDataProvider;
import com.csform.android.uiapptemplate2.utils.HidingScrollListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;



public class CardRateFragment extends Fragment {

    private List<CardsRateDataProvider> rateList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CardsRateAdapter mAdapter;

    private StorageReference mStorageRef;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        // getLayoutInflater().inflate(R.layout.recyclerview_card_profile, contentFrameLayout);
        return inflater.inflate(R.layout.recyclerview_cards, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        SharedPreferences subscribe = getActivity().getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_cards);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        mAdapter = new CardsRateAdapter(rateList);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setNestedScrollingEnabled(false);


        if (firebaseStatus == 1) {
            final ArrayList<CardsRateDataProvider> theCardsRateList = new ArrayList<>();

            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            final DatabaseReference ref1 = database1.getReference("cards/rate");


            ref1.addChildEventListener(new ChildEventListener() {

                @Override
                public void onChildAdded(final DataSnapshot dataSnapshot, String s) {


                    final CardsRateDataProvider cardRate = dataSnapshot.getValue(CardsRateDataProvider.class);


                    if (cardRate.getAvatar_image_url() == null || cardRate.getAvatar_image_url().equals("")) {

                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/rate/" + cardRate.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                            @Override
                            public void onSuccess(Uri uri) {


                                String uristring = uri.toString();
                                cardRate.setAvatar_image_url(uristring);
                                ref1.child(dataSnapshot.getKey()).setValue(cardRate);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {


                            }
                        });
                    }

                    theCardsRateList.add(cardRate);

                    //setCategoriesData(count);

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        mAdapter = new CardsRateAdapter(theCardsRateList);
                                                        rateList = theCardsRateList;
                                                        recyclerView.setAdapter(null);
                                                        recyclerView.setAdapter(mAdapter);
                                                        //recyclerView.getAdapter().notifyDataSetChanged();

                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );

        } else {
            mAdapter = new CardsRateAdapter(rateList);
            prepareRateData();
            recyclerView.setAdapter(mAdapter);
        }
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(R.id
                .coordinatorLayout);
        try {
            final FloatingActionButton fam = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            fam.setVisibility(View.VISIBLE);
            fam.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "FAB Button pressed", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }
            });
            final Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

            recyclerView.addOnScrollListener(new HidingScrollListener() {
                @Override
                public void onHide() {
                    CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fam.getLayoutParams();
                    int fabBottomMargin = lp.bottomMargin;
                    fam.animate().translationY(fam.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
                }

                @Override
                public void onShow() {
                    toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                    fam.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
                }
            });
        } catch (Exception e) {
            return;
        }
        ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {
            //and in your imlpementaion of
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                // get the viewHolder's and target's positions in your adapter data, swap them
                Collections.swap(rateList, viewHolder.getAdapterPosition(), target.getAdapterPosition());
                // and notify the adapter that its dataset has changed
                mAdapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
                //return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                rateList.remove(viewHolder.getAdapterPosition());
                recyclerView.getAdapter().notifyItemRemoved(viewHolder.getAdapterPosition());
            }

            //defines the enabled move directions in each state (idle, swiping, dragging).
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                //return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                //        ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);

            }
        };
        ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
        ith.attachToRecyclerView(recyclerView);


    }

    private void prepareRateData() {
        CardsRateDataProvider rate = new CardsRateDataProvider("Juila Petersen", "Aboris exercitation veniam laboris incididunt duis aliquip.", "6.3", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Holman Valencia", "Llamco nisi sunt velit quis sint anim nisi sunt Lorem in.", "9.3", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Marisa Cain", "Ulpa aliqua sint incididunt consectetur deserunt excepteur voluptate non excepteur et esse labore.", "8.6", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Dejesus Norris", "Labore dolor in sint.", "9.2", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Gayle Gaines", "St quis consequat laboris pariatur magna.", "7.6", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Prince Phelps", "Iusmod enim sunt adipisicing ex enim id reprehenderit eiusmod quis non ut.", "8.6", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Keri Hudson", "Cupidatat voluptate do qui pariatur.", "5.2", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Duran Clayton", "Met do mollit quis ex nostrud.", "7.6", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Schmidt English", "Tempor do minim eiusmod aliquip tempor reprehenderit aliquip nulla consectetur qui.", "9.3", R.drawable.ic_image);
        rateList.add(rate);
        rate = new CardsRateDataProvider("Lara Lynn", "Roident est duis duis sit occaecat ea eiusmod laboris mollit ullamco mollit nisi veniam.", "8.8", R.drawable.ic_image);
        rateList.add(rate);


        mAdapter.notifyDataSetChanged();
    }

}
