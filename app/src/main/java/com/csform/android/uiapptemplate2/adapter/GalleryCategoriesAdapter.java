package com.csform.android.uiapptemplate2.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.squareup.picasso.Picasso;

import java.util.Random;

public class GalleryCategoriesAdapter extends ArrayAdapter<String> {

    private Activity context;
    private String[] itemname;
    private String[] imgid;
    private Integer[] numimages;
    private int[] category_images;


    public GalleryCategoriesAdapter(Activity context, String[] itemname, int[] category_images, Integer[] numimages) {
        super(context, R.layout.left_menu, itemname);
        this.context = context;
        this.itemname = itemname;
        shuffleArray(category_images);
        this.category_images = category_images;
        this.numimages = numimages;
    }

    public GalleryCategoriesAdapter(Activity context, String[] itemname, String[] imgid, Integer[] numimages) {
        super(context, R.layout.left_menu, itemname);


        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
        this.numimages = numimages;
    }

    public void updateData(String[] imgid) {


        this.imgid = imgid;

    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_categories, parent,false);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.item);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            holder.txtNumber=(TextView) convertView.findViewById(R.id.textNumber);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(itemname[position]);
        String photos = Integer.toString(numimages[position]) + " " + this.context.getString(R.string.photos);
        holder.txtNumber.setText(photos);

        if (imgid != null) {


            Picasso.with(getContext())
                    .load(imgid[position])
                    .error(R.drawable.default_image)
                    //.placeholder(R.drawable.ic_trash)
                    .fit()
                    .into(holder.imageView);
        } else {
            Picasso.with(getContext())
                    .load(category_images[position])
                    .error(R.drawable.default_image)
                    //    .placeholder(R.drawable.ic_trash)
                    .fit()
                    .into(holder.imageView);
        }
        return convertView;

    }

    private static void shuffleArray(int[] array) {
        int index;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            if (index != i) {
                array[index] ^= array[i];
                array[i] ^= array[index];
                array[index] ^= array[i];
            }
        }
    }
    private static class ViewHolder {
        private TextView txtTitle;
        private TextView txtNumber;
        private ImageView imageView;
    }

}