package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;



public class DialogSubscribe extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_subscribe);
        this.setFinishOnTouchOutside(false);
        final TextView cancel = (TextView) findViewById(R.id.dialog_subscribe_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        final TextView subscribe = (TextView) findViewById(R.id.dialog_subscribe_submit);
        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputLayout editEmail = (TextInputLayout) findViewById(R.id.dialog_subscribe_email);
                EditText enteredEmail = (EditText) findViewById(R.id.dialog_subscribe_enteredEmail);
                String email = enteredEmail.getText().toString();

                if (isValidEmail(email)) {

                    finish();
                } else {

                    editEmail.setError(getString(R.string.dialog_subscribe_error));
                }


                //finish();

            }
        });

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
