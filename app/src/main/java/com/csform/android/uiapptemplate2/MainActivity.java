package com.csform.android.uiapptemplate2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getExtras() != null) {
            String wizzard = getIntent().getStringExtra("wizzard");
            if (wizzard != null && wizzard.equals("1")) {
                Intent wizzardIntent = new Intent(MainActivity.this, WizzardIntro.class);
                //startActivity(wizzardIntent);
            }
        }

        if (getIntent().getStringExtra("position") == null && getIntent().getStringExtra("nowizzard") == null) {
            Intent wizzardIntent = new Intent(MainActivity.this, WizzardIntro.class);
            //egor
//            startActivity(wizzardIntent);
        }


        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText("HOME");
        if (getIntent().getExtras() != null && (getIntent().getStringExtra("position") != null)) {

            selectItem(Integer.parseInt(getIntent().getStringExtra("position")), 0);

        } else {

            selectItem(0, 0);
        }

        SharedPreferences subscribe = getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);

        if (!isOnline() && firebaseStatus == 1) {
            Intent myIntent = new Intent(this, DialogWarning.class);
            myIntent.putExtra("key", "internet"); //Optional parameters
            startActivity(myIntent);
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);


    }


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


}
