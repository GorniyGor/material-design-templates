package com.csform.android.uiapptemplate2;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;


public class ValidationRegister extends BaseActivityTransparentToolbar {
    private AppBarLayout appbar;
    private ConstraintLayout main;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        orientation = 1;
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.validation_register_content, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.validation_register_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar_transparent);
        toolbar.getBackground().setAlpha(200);
        //drawerLayout.setAlpha(1);
        appbar = (AppBarLayout) findViewById(R.id.app_bar_transparent);
        appbar.getBackground().setAlpha(0);

        main = (ConstraintLayout) findViewById(R.id.content_top);
       /* main.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                ConstraintLayout.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);*/

        final TextView subscribe = (TextView) findViewById(R.id.login_validation);
        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputLayout editEmail = (TextInputLayout) findViewById(R.id.validation_email);
                EditText enteredEmail = (EditText) findViewById(R.id.validation_email_edit);
                String email = enteredEmail.getText().toString();

                if (isValidEmail(email)) {

                    // finish();
                } else {

                    editEmail.setError(getString(R.string.dialog_subscribe_error));
                }
                TextInputLayout editPassword = (TextInputLayout) findViewById(R.id.validation_password);
                EditText enteredPassword = (EditText) findViewById(R.id.validation_password_edit);
                String password = enteredPassword.getText().toString();
                if (password.length() > 1) {

                    // finish();
                } else {

                    editPassword.setError(getString(R.string.required));
                }
                TextInputLayout editCountry = (TextInputLayout) findViewById(R.id.validation_country);
                EditText enteredCountry = (EditText) findViewById(R.id.validation_country_edit);
                String country = enteredCountry.getText().toString();
                if (country.length() > 1) {

                    // finish();
                } else {

                    editCountry.setError(getString(R.string.required));
                }
                TextInputLayout editZip = (TextInputLayout) findViewById(R.id.validation_zip);
                EditText enteredZip = (EditText) findViewById(R.id.validation_zip_edit);
                String zip = enteredZip.getText().toString();
                if (zip.length() > 1) {

                    // finish();
                } else {

                    editZip.setError(getString(R.string.required));
                }
                TextInputLayout editUsername = (TextInputLayout) findViewById(R.id.validation_username);
                EditText enteredUsername = (EditText) findViewById(R.id.validation_username_edit);
                String username = enteredUsername.getText().toString();
                if (username.length() > 1) {

                    // finish();
                } else {

                    editUsername.setError(getString(R.string.required));
                }


                //finish();

            }

        });
        final TextView login = (TextView) findViewById(R.id.register_loginsubmit);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), ValidationLogin.class);
                startActivity(intent);

                //finish();

            }
        });


    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
