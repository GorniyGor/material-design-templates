package com.csform.android.uiapptemplate2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.CardsRateDataProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;




public class CardsRateAdapter extends RecyclerView.Adapter<CardsRateAdapter.MyViewHolder> {

    private List<CardsRateDataProvider> ratesList;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, about, rate;
        private ImageView avatar_image;
        private ImageView rate_image;
        public Context context;


        private MyViewHolder(final View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtName);
            about = (TextView) view.findViewById(R.id.txtAbout);
            rate = (TextView) view.findViewById(R.id.textRate);

            rate_image = (ImageView) view.findViewById(R.id.rate_image);


            avatar_image = (ImageView) view.findViewById(R.id.imgAvatar);
            Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
            avatar_image.setImageDrawable(transparentDrawable);
            rate_image.setImageDrawable(transparentDrawable);


        }
    }


    public CardsRateAdapter(List<CardsRateDataProvider> ratesList) {
        this.ratesList = ratesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_rate, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CardsRateDataProvider profile = ratesList.get(position);
        holder.name.setText(profile.getCard_title());
        holder.about.setText(profile.getCard_subtitle());
        holder.rate.setText(profile.getRate());

        StorageReference mStorageRef;
        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        holder.avatar_image.setImageDrawable(transparentDrawable);

        if (profile.getPicture() != null) {

            holder.avatar_image.setImageResource(profile.getPicture());

            holder.rate_image.setImageResource(R.drawable.ic_star_grey);
        } else {
            if (profile.getAvatar_image_url() == null || profile.getAvatar_image_url().equals("")) {

                if (profile.getAvatar_image_name() != null) {


                    if (!profile.getAvatar_image_name().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/rate/" + profile.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                profile.setAvatar_image_url(uristring);
                                this.result = uristring;
                                Picasso
                                        .with(holder.avatar_image.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.avatar_image);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.avatar_image.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {

                        // holder.avatar_image.setVisibility(View.INVISIBLE);

                        holder.avatar_image.setImageDrawable(transparentDrawable);
                    }

                } else {

                    // holder.avatar_image.setVisibility(View.INVISIBLE);

                    holder.avatar_image.setImageDrawable(transparentDrawable);
                }
            } else {
                Picasso
                        .with(holder.avatar_image.getContext())
                        .load(profile.getAvatar_image_url())
                        .fit() // will explain later
                        .into(holder.avatar_image);
            }


            if (profile.getRate_image_url() == null) {
                if (profile.getRate_image() != null) {

                    if (!profile.getRate_image().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/rate/" + profile.getRate_image()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.rate_image.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.rate_image);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.rate_image.setImageResource(R.drawable.default_image);


                            }
                        });

                    }
                } else {


                    holder.rate_image.setImageDrawable(transparentDrawable);
                }
            } else {
                Picasso
                        .with(holder.avatar_image.getContext())
                        .load(profile.getRate_image_url())
                        .fit() // will explain later
                        .into(holder.avatar_image);
            }
        }

    }

    @Override
    public int getItemCount() {
        return ratesList.size();
    }
}