package com.csform.android.uiapptemplate2.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.csform.android.uiapptemplate2.GalleryCategories;
import com.csform.android.uiapptemplate2.data.CardsFollowDataProvider;
import com.csform.android.uiapptemplate2.data.CardsProfileDataProvider;
import com.csform.android.uiapptemplate2.data.CardsRateDataProvider;
import com.csform.android.uiapptemplate2.data.DraggableDataProvider;
import com.csform.android.uiapptemplate2.data.ExpandableDataProvider;
import com.csform.android.uiapptemplate2.data.ExpendableDraggableSwipeableDataProvider;
import com.csform.android.uiapptemplate2.data.ListItemDetailsDataProvider;
import com.csform.android.uiapptemplate2.data.SwipableDataProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import static android.content.Context.MODE_PRIVATE;


public class RefreshImagesUrl {


    public RefreshImagesUrl() {


    }

    public static void refresh(Context context) {

        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        SharedPreferences subscribe = context.getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);
        if (firebaseStatus == 1)

        {


            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            final DatabaseReference ref1 = database1.getReference("cards/follow");

            ref1.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(final DataSnapshot dataSnapshot, String s) {


                    final CardsFollowDataProvider cardFollow = dataSnapshot.getValue(CardsFollowDataProvider.class);
                    mStorageRef.child("cards/follow/" + cardFollow.getAvatar_image_name_1()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String uristring = uri.toString();
                            cardFollow.setAvatar_image_name_1_url(uristring);
                            ref1.child(dataSnapshot.getKey()).setValue(cardFollow);


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                        }
                    });


                    mStorageRef.child("cards/follow/" + cardFollow.getAvatar_image_name_2()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

                        @Override
                        public void onSuccess(Uri uri) {


                            String uristring = uri.toString();
                            cardFollow.setAvatar_image_name_2_url(uristring);
                            ref1.child(dataSnapshot.getKey()).setValue(cardFollow);


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {


                        }
                    });


                    mStorageRef.child("cards/follow/" + cardFollow.getAvatar_image_name_3()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        String result;

                        @Override
                        public void onSuccess(Uri uri) {


                            String uristring = uri.toString();
                            cardFollow.setAvatar_image_name_3_url(uristring);
                            ref1.child(dataSnapshot.getKey()).setValue(cardFollow);


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {


                        }
                    });


                    mStorageRef.child("cards/follow/" + cardFollow.getAvatar_image_name_4()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        String result;

                        @Override
                        public void onSuccess(Uri uri) {


                            String uristring = uri.toString();
                            cardFollow.setAvatar_image_name_4_url(uristring);
                            ref1.child(dataSnapshot.getKey()).setValue(cardFollow);


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {


                        }
                    });


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        refreshCardsProfile();


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );


        }
    }

    public static void refreshCardsProfile() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref2 = database1.getReference("cards/profile");

        ref2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {


                final CardsProfileDataProvider cardProfile = dataSnapshot.getValue(CardsProfileDataProvider.class);


                mStorageRef.child("cards/profile/" + cardProfile.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    String result;

                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        cardProfile.setAvatar_image_url(uristring);
                        ref2.child(dataSnapshot.getKey()).setValue(cardProfile);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


                //setCategoriesData(count);
                //count++;

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref2.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    refreshCardsRate();


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );

    }

    public static void refreshCardsRate() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref3 = database1.getReference("cards/rate");


        ref3.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {


                final CardsRateDataProvider cardRate = dataSnapshot.getValue(CardsRateDataProvider.class);


                mStorageRef.child("cards/rate/" + cardRate.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    String result;

                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        cardRate.setAvatar_image_url(uristring);
                        ref3.child(dataSnapshot.getKey()).setValue(cardRate);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref3.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    refreshListsExpendable();


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );

    }

    private static void refreshListsExpendable() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();

        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref4 = database1.getReference("Lists/expandable");


        ref4.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                final ExpandableDataProvider.ListDataGroup listData = dataSnapshot.getValue(ExpandableDataProvider.ListDataGroup.class);


                mStorageRef.child("lists/expandable/" + listData.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setAvatar_image_url(uristring);
                        ref4.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref4.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    refreshListsDragDrop();


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );
    }

    private static void refreshListsDragDrop() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref5 = database1.getReference("Lists/drag_drop");


        ref5.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                final DraggableDataProvider.ListData listData = dataSnapshot.getValue(DraggableDataProvider.ListData.class);


                mStorageRef.child("lists/drag_drop/" + listData.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setAvatar_image_url(uristring);
                        ref5.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref5.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    refreshListsItemDetails();


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );
    }

    private static void refreshListsItemDetails() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref6 = database1.getReference("Lists/item_details");


        ref6.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                final ListItemDetailsDataProvider listData = dataSnapshot.getValue(ListItemDetailsDataProvider.class);


                mStorageRef.child("lists/item_details/" + listData.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setAvatar_image_url(uristring);
                        ref6.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref6.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    refreshListSwipe();


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );
    }

    private static void refreshListSwipe() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref7 = database1.getReference("Lists/swipe_to_dismiss");
        ref7.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                final SwipableDataProvider.ListData listData = dataSnapshot.getValue(SwipableDataProvider.ListData.class);


                mStorageRef.child("lists/swipe/" + listData.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {

                        String uristring = uri.toString();
                        listData.setAvatar_image_url(uristring);
                        ref7.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref7.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    final Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            refreshListExpDragDrop();
                                                        }
                                                    }, 10000);


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );
    }

    private static void refreshListExpDragDrop() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref8 = database1.getReference("Lists/exp_drag_drop");
        ref8.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                final ExpendableDraggableSwipeableDataProvider.ListDataGroup listData = dataSnapshot.getValue(ExpendableDraggableSwipeableDataProvider.ListDataGroup.class);


                mStorageRef.child("lists/swipe_drag_expand/" + listData.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setAvatar_image_url(uristring);
                        ref8.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });
                mStorageRef.child("lists/swipe_drag_expand/" + listData.getLocation_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setLocation_image_url(uristring);
                        ref8.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });
                mStorageRef.child("lists/swipe_drag_expand/" + listData.getRating_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setRating_image_url(uristring);
                        ref8.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref8.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    final Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            refreshGallery();
                                                        }
                                                    }, 5000);


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );
    }

    private static void refreshGallery() {
        final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference ref9 = database1.getReference("Gallery");


        ref9.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

            //    final ListItemDetailsDataProvider listData = dataSnapshot.getValue(ListItemDetailsDataProvider.class);

               final  GalleryCategories.PictureWithCategory listData = dataSnapshot.getValue(GalleryCategories.PictureWithCategory.class);

                /*

                                mStorageRef.child("gallery/"+ listPicturesWithCategories.get(current).getCategory()+"/"+ listPicturesWithCategories.get(current).getName()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {


                 */
                System.out.println();
                mStorageRef.child("gallery/" + listData.getCategory()+"/"+ listData.getImage_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                    @Override
                    public void onSuccess(Uri uri) {


                        String uristring = uri.toString();
                        listData.setImage_url(uristring);
                        ref9.child(dataSnapshot.getKey()).setValue(listData);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref9.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {



                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            }
        );
    }



}
