package com.csform.android.uiapptemplate2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.utils.ExtendedViewPager;
import com.csform.android.uiapptemplate2.utils.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



public class GalleryFullScreen extends BaseActivityTransparentToolbar {
    private static final int FIREBASE_ENABLED = 1;
    private static String[] images = {};
    private static Integer[] imagesInt = {};
    private static String[] images_names = {};
    public static final String EXTRA_MESSAGE = "Gallery Category";

    String[] imgUrl;
    Integer[] imgInt;
    String[] imgUrl_names;
    List<String> listImgUrl = new ArrayList<String>();
    private AppBarLayout appbar;
    private String imageUri;
    private String category;
    private Context context;
    private int pictureposition;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        orientation = 1;
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        contentFrameLayout.removeAllViews();

        getLayoutInflater().inflate(R.layout.gallery_fullscreen, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);

        toolbar = (Toolbar) findViewById(R.id.toolbar_transparent);
        toolbar.getBackground().setAlpha(0);
        //drawerLayout.setAlpha(1);
        appbar = (AppBarLayout) findViewById(R.id.app_bar_transparent);
        appbar.getBackground().setAlpha(0);


        ExtendedViewPager mViewPager = (ExtendedViewPager) findViewById(R.id.view_pager);


        context = getApplicationContext();


        Intent intent = getIntent();
        imageUri = intent.getStringExtra(GalleryCategory.EXTRA_MESSAGE);
        category = intent.getStringExtra(GalleryCategory.EXTRA_CATEGORY);
        toolbartitle.setText(category);

        if (intent.getStringExtra("position") != null) {
            pictureposition = Integer.parseInt(intent.getStringExtra("position"));
        } else
            pictureposition = 0;
        if (imageUri == null || imageUri.equals("")) {
            imageUri = "https://images.pexels.com/photos/67843/splashing-splash-aqua-water-67843.jpeg?w=940&h=650&auto=compress&cs=tinysrgb";
        }
        if (category == null || category.equals("") || category.equals("CATEGORY")) {
            category = "CATEGORY";
        }

        if (intent.getStringArrayListExtra("pictures_names") != null) {
            ArrayList<String> pictures_names = intent.getStringArrayListExtra("pictures_names");


            imgUrl_names = new String[pictures_names.size()];


            pictures_names.toArray(imgUrl_names);


            images_names = imgUrl_names;


        }
        if (intent.getStringArrayListExtra("pictures_url") != null) {
            ArrayList<String> pictures = intent.getStringArrayListExtra("pictures_url");


            imgUrl = new String[pictures.size()];

            pictures.toArray(imgUrl);


            images = imgUrl;
            mViewPager.setAdapter(new TouchImageAdapter(images));


        } else {


            ArrayList<Integer> pictures_local = intent.getIntegerArrayListExtra("pictures_local");


            imgInt = new Integer[pictures_local.size()];

            pictures_local.toArray(imgInt);


            imagesInt = imgInt;
            mViewPager.setAdapter(new TouchImageAdapter(imagesInt));


        }


        //images = imgUrl;


        final TextView name = (TextView) findViewById(R.id.image_name);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                name.setText(images_names[position]);

            }

            public void onPageSelected(int position) {

                // Check if this is the page you want.
            }
        });
        mViewPager.setCurrentItem(pictureposition);


    }

    @Override
    public void onBackPressed() {
        if (!menuclicked) {

            Intent intent = new Intent(this, GalleryCategory.class);
            //String message = categoryName[position];
            intent.putExtra(EXTRA_MESSAGE, category);
            startActivity(intent);
        } else
            super.onBackPressed();
    }


    static class TouchImageAdapter extends PagerAdapter {
        private String[] images;
        private Integer[] local_images;

        public TouchImageAdapter(String[] images) {
            this.images = images;
        }

        public TouchImageAdapter(Integer[] local_images) {
            this.local_images = local_images;
        }

        @Override
        public int getCount() {
            if (images != null)
                return images.length;
            else
                return local_images.length;
        }


        @Override
        public View instantiateItem(ViewGroup container, int position) {


            TouchImageView img = new TouchImageView(container.getContext());
            if (images != null) {
                Picasso.with(img.getContext())
                        .load(images[position])
                        .error(R.drawable.default_image)
                        //  .placeholder(R.drawable.default_image)
                        .into(img);
            } else {
                Picasso.with(img.getContext())
                        .load(local_images[position])
                        .error(R.drawable.default_image)
                        .into(img);
            }
            //  img.setImageResource(images[position]);
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }


}
