/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.Pair;

import com.csform.android.uiapptemplate2.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ExpandableDataProvider extends AbstractDataProviderExpandable {
    private List<Pair<GroupData, List<ChildData>>> mData;

    // for undo group item
    private Pair<GroupData, List<ChildData>> mLastRemovedGroup;
    private int mLastRemovedGroupPosition = -1;

    // for undo child item
    private ChildData mLastRemovedChild;
    private long mLastRemovedChildParentGroupId = -1;
    private int mLastRemovedChildPosition = -1;

    private Context context;
    private Integer firebase_enabled = 0;
    private volatile Integer firebase_loaded;
    private StorageReference mStorageRef;

    public ExpandableDataProvider(Context context) {
        this.context = context;

        SharedPreferences subscribe = context.getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        firebase_enabled = subscribe.getInt("firebase_enabled", 0);

        mData = new LinkedList<>();

        if (firebase_enabled == 1) {

            firebase_loaded = 0;
            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            DatabaseReference ref1 = database1.getReference("Lists/expandable");

            ref1.addChildEventListener(new ChildEventListener() {
                int count = 0;

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    ListDataGroup listData = dataSnapshot.getValue(ListDataGroup.class);
                    System.out.println("prvi");
                    System.out.println(listData.item_title);

                    final ConcreteGroupData group = new ConcreteGroupData(count, listData.span, listData.item_title, listData.item_subtitle, listData.avatar_image_name, listData.avatar_image_url);
                    final List<ChildData> children = new ArrayList<>();
                    long childId = group.generateNewChildId();
                    children.add(new ConcreteChildData(childId, listData.subitem_title_1, listData.subitem_subtitle_1));
                    childId = group.generateNewChildId();
                    System.out.println("zar");
                    System.out.println(listData.getItem_title());
                    System.out.println(listData.subitem_subtitle_1);
                    System.out.println(listData.subitem_subtitle_2);
                    children.add(new ConcreteChildData(childId, listData.subitem_title_2, listData.subitem_subtitle_2));
                    mData.add(new Pair<GroupData, List<ChildData>>(group, children));

                    count++;


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {


                                                        firebase_loaded = 1;


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        } else {


            ConcreteGroupData group = new ConcreteGroupData(0, "(1927)", "Grant Marshall", "marshall@yahoo.com", R.drawable.ic_avatarcaptain);

            List<ChildData> children = new ArrayList<>();


            long childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Barrera Ramsey", "+1 (861) 550-2796"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Perry Bradley", "+1 (901) 477-3644"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));


            group = new ConcreteGroupData(1, "(1983)", "Pena Valdez", "valdez@yahoo.com", R.drawable.ic_avatarshowman);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Holman Valencia", "+1 (909) 445-2527"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Rhett Tyson", "+1 (824) 571-2420"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));
            group = new ConcreteGroupData(2, "(1980)", "Jessica Miles", "miles@mail.com", R.drawable.ic_avatardisc_jockey);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Indigo Peters", "+1 (909) 445-2527"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Carson Brooks", "+1 (989) 567-2136"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(3, "(1975)", "Kerri Barber", "barber@gmail.com", R.drawable.ic_avatarastronaut);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Lyle Hall", "+1 (929) 475-3153"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Kevyn Coupe", "+1 (905) 542-2489"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));
            group = new ConcreteGroupData(4, "(1985)", "Natasha Gamble", "gamble@outlook.com", R.drawable.ic_avatardetective);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Alfie Bennett", "+1 (980) 424-3729"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Delmar Emerson", "+1 (927) 524-3369"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));
            group = new ConcreteGroupData(5, "(1995)", "White Castaneda", "castaneda@mail.com", R.drawable.ic_avatardisc_jockey);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Fredric Outterridge", "+1 (863) 537-3885"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Ashton Thorley", "+1 (943) 572-2116"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(6, "(1984)", "Vanessa Ryan", "ryan@mail.com", R.drawable.ic_avatardiver);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Elwin Eldridge", "+1 (901) 405-2392"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Kennard Wright", "+1 (900) 451-3239"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));
            group = new ConcreteGroupData(7, "(1960)", "Meredith Hendricks", "hendricks@yahoo.com", R.drawable.ic_avatardetective);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Beauregard Gladwyn", "+1 (989) 567-2420"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Fenton Martel", "+1 (846) 466-3723"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(8, "(1977)", "Carol Kelly", "kelly@mail.com", R.drawable.ic_avatardiver);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Raphael Averill", "+1 (946) 428-3636"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Cale Rupertson", "+1 (872) 437-3119"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(9, "(1988)", "Barrera Ramsey", "ramsey@gmail.com", R.drawable.ic_avatarshowman);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Geordie Morin", "+1 (901) 477-3644"));
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "Ellis Thorne", "+1 (825) 558-2639"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));


        }


    }

    @Override
    public Integer getFirebase_enabled() {
        return firebase_enabled;
    }

    @Override
    public Integer getFirebase_loaded() {

        return firebase_loaded;
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mData.get(groupPosition).second.size();
    }

    @Override
    public GroupData getGroupItem(int groupPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            throw new IndexOutOfBoundsException("groupPosition = " + groupPosition);
        }

        return mData.get(groupPosition).first;
    }

    @Override
    public ChildData getChildItem(int groupPosition, int childPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            throw new IndexOutOfBoundsException("groupPosition = " + groupPosition);
        }

        final List<ChildData> children = mData.get(groupPosition).second;

        if (childPosition < 0 || childPosition >= children.size()) {
            throw new IndexOutOfBoundsException("childPosition = " + childPosition);
        }

        return children.get(childPosition);
    }

    @Override
    public void moveGroupItem(int fromGroupPosition, int toGroupPosition) {
        if (fromGroupPosition == toGroupPosition) {
            return;
        }

        final Pair<GroupData, List<ChildData>> item = mData.remove(fromGroupPosition);
        mData.add(toGroupPosition, item);
    }

    @Override
    public void moveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition) {
        if ((fromGroupPosition == toGroupPosition) && (fromChildPosition == toChildPosition)) {
            return;
        }

        final Pair<GroupData, List<ChildData>> fromGroup = mData.get(fromGroupPosition);
        final Pair<GroupData, List<ChildData>> toGroup = mData.get(toGroupPosition);

        final ConcreteChildData item = (ConcreteChildData) fromGroup.second.remove(fromChildPosition);

        if (toGroupPosition != fromGroupPosition) {
            // assign a new ID
            final long newId = ((ConcreteGroupData) toGroup.first).generateNewChildId();
            item.setChildId(newId);
        }

        toGroup.second.add(toChildPosition, item);
    }

    @Override
    public void removeGroupItem(int groupPosition) {
        mLastRemovedGroup = mData.remove(groupPosition);
        mLastRemovedGroupPosition = groupPosition;

        mLastRemovedChild = null;
        mLastRemovedChildParentGroupId = -1;
        mLastRemovedChildPosition = -1;
    }

    @Override
    public void removeChildItem(int groupPosition, int childPosition) {
        mLastRemovedChild = mData.get(groupPosition).second.remove(childPosition);
        mLastRemovedChildParentGroupId = mData.get(groupPosition).first.getGroupId();
        mLastRemovedChildPosition = childPosition;

        mLastRemovedGroup = null;
        mLastRemovedGroupPosition = -1;
    }


    @Override
    public long undoLastRemoval() {
        if (mLastRemovedGroup != null) {
            return undoGroupRemoval();
        } else if (mLastRemovedChild != null) {
            return undoChildRemoval();
        } else {
            return RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION;
        }
    }

    private long undoGroupRemoval() {
        int insertedPosition;
        if (mLastRemovedGroupPosition >= 0 && mLastRemovedGroupPosition < mData.size()) {
            insertedPosition = mLastRemovedGroupPosition;
        } else {
            insertedPosition = mData.size();
        }

        mData.add(insertedPosition, mLastRemovedGroup);

        mLastRemovedGroup = null;
        mLastRemovedGroupPosition = -1;

        return RecyclerViewExpandableItemManager.getPackedPositionForGroup(insertedPosition);
    }

    private long undoChildRemoval() {
        Pair<GroupData, List<ChildData>> group = null;
        int groupPosition = -1;

        // find the group
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).first.getGroupId() == mLastRemovedChildParentGroupId) {
                group = mData.get(i);
                groupPosition = i;
                break;
            }
        }

        if (group == null) {
            return RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION;
        }

        int insertedPosition;
        if (mLastRemovedChildPosition >= 0 && mLastRemovedChildPosition < group.second.size()) {
            insertedPosition = mLastRemovedChildPosition;
        } else {
            insertedPosition = group.second.size();
        }

        group.second.add(insertedPosition, mLastRemovedChild);

        mLastRemovedChildParentGroupId = -1;
        mLastRemovedChildPosition = -1;
        mLastRemovedChild = null;

        return RecyclerViewExpandableItemManager.getPackedPositionForChild(groupPosition, insertedPosition);
    }

    public static final class ConcreteGroupData extends GroupData {

        private final long mId;
        private final Integer mAvatar;
        private final String mAvatar_image_name;
        private final String mAvatar_image_url;
        private final String mName;
        private final String mYear;
        private final String mEmail;

        private boolean mPinned;
        private long mNextChildId;

        ConcreteGroupData(long id, String text, String name, String email, int avatar) {
            mId = id;
            mYear = text;
            mName = name;
            mEmail = email;
            mAvatar = avatar;
            mNextChildId = 0;
            mAvatar_image_name = "";
            mAvatar_image_url = "";
        }

        ConcreteGroupData(long id, String year, String item_title, String item_subtitle, String avatar_image_name, String avatar_image_url) {

            mId = id;
            mYear = year;
            mName = item_title;
            mEmail = item_subtitle;
            mAvatar = null;
            mAvatar_image_name = avatar_image_name;
            mAvatar_image_url = avatar_image_url;
            mNextChildId = 0;
        }

        @Override
        public long getGroupId() {
            return mId;
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public String getName() {
            return mName;
        }

        @Override
        public String getGroupEmail() {
            return mEmail;
        }

        @Override
        public String getGroupAvatar_link() {
            return mAvatar_image_name;
        }

        @Override
        public Integer getGroupAvatar() {
            return mAvatar;
        }

        @Override
        public String getAvatar_image_url() {
            return mAvatar_image_url;
        }

        @Override
        public String getGroupYear() {
            return mYear;
        }

        @Override
        public void setPinned(boolean pinnedToSwipeLeft) {
            mPinned = pinnedToSwipeLeft;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        public long generateNewChildId() {
            final long id = mNextChildId;
            mNextChildId += 1;
            return id;
        }
    }

    public static final class ConcreteChildData extends ChildData {

        private long mId;
        private final String mText;
        private final String mTelephone;

        private boolean mPinned;

        ConcreteChildData(long id, String text, String telephone) {
            mId = id;
            mText = text;
            mTelephone = telephone;
            System.out.println("text");
            System.out.println(text);
            System.out.println(telephone);
        }

        @Override
        public long getChildId() {
            return mId;
        }

        @Override
        public String getName() {
            return mText;
        }


        @Override
        public String getChildTelephone() {
            return mTelephone;
        }

        @Override
        public String getGroupYear() {
            return null;
        }

        @Override
        public String getAvatar_image_url() {
            return null;
        }

        public String getGroupAvatar_link() {
            return null;
        }

        public String getGroupEmail() {
            return null;
        }

        public Integer getGroupAvatar() {
            return null;
        }


        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        public void setChildId(long id) {
            this.mId = id;
        }
    }

    public static final class ListDataGroup {


        String avatar_image_name;
        String avatar_image_url;
        String item_title;
        String span;
        String item_subtitle;
        String subitem_subtitle_1;
        String subitem_subtitle_2;
        String subitem_title_1;
        String subitem_title_2;


        ListDataGroup() {
        }

        ListDataGroup(String avatar_image_name, String avatar_image_url, String item_title, String span, String item_subtitle, String subitem_title_1, String subitem_title_2,
                      String subitem_subtitle_1, String subitem_subtitle_2) {
            this.avatar_image_name = avatar_image_name;
            this.avatar_image_url = avatar_image_url;
            this.item_title = item_title;
            this.span = span;
            this.item_subtitle = item_subtitle;
            this.subitem_title_1 = subitem_title_1;
            this.subitem_title_2 = subitem_title_2;
            this.subitem_subtitle_1 = subitem_subtitle_1;
            this.subitem_subtitle_2 = subitem_subtitle_2;
            System.out.println("idemo");
            System.out.println(subitem_subtitle_1);
            System.out.println(subitem_subtitle_2);


        }

        public String getSubitem_subtitle_1() {
            return subitem_subtitle_1;
        }

        public void setSubitem_subtitle_1(String subitem_subtitle_1) {
            this.subitem_subtitle_1 = subitem_subtitle_1;
        }

        public String getSubitem_subtitle_2() {
            return subitem_subtitle_2;
        }

        public void setSubitem_subtitle_2(String subitem_subtitle_2) {
            this.subitem_subtitle_2 = subitem_subtitle_2;
        }

        public String getSubitem_title_1() {
            return subitem_title_1;
        }

        public void setSubitem_title_1(String subitem_title_1) {
            this.subitem_title_1 = subitem_title_1;
        }

        public String getSubitem_title_2() {
            return subitem_title_2;
        }

        public void setSubitem_title_2(String subitem_title_2) {
            this.subitem_title_2 = subitem_title_2;
        }

        public String getAvatar_image_url() {
            return avatar_image_url;
        }

        public void setAvatar_image_url(String avatar_image_url) {
            this.avatar_image_url = avatar_image_url;
        }

        public String getItem_subtitle() {
            return item_subtitle;
        }

        public void setItem_subtitle(String item_subtitle) {
            this.item_subtitle = item_subtitle;
        }

        public String getSpan() {

            return span;
        }


        public void setSpan(String span) {
            this.span = span;
        }

        public String getAvatar_image_name() {
            return avatar_image_name;
        }

        public void setAvatar_image_name(String avatar_image_name) {
            this.avatar_image_name = avatar_image_name;
        }

        public String getItem_title() {
            return item_title;
        }

        public void setItem_title(String item_title) {
            this.item_title = item_title;
        }
    }


}
