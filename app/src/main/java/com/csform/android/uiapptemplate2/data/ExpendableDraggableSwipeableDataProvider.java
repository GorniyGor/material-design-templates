/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.Pair;

import com.csform.android.uiapptemplate2.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ExpendableDraggableSwipeableDataProvider extends AbstractDataProviderExpendableDraggableSwipeable {
    private List<Pair<GroupData, List<ChildData>>> mData;


    private Context context;
    private Integer firebase_enabled = 0;
    private volatile Integer firebase_loaded;
    private StorageReference mStorageRef;

    // for undo group item
    private Pair<GroupData, List<ChildData>> mLastRemovedGroup;
    private int mLastRemovedGroupPosition = -1;

    // for undo child item
    private ChildData mLastRemovedChild;
    private long mLastRemovedChildParentGroupId = -1;
    private int mLastRemovedChildPosition = -1;

    public ExpendableDraggableSwipeableDataProvider(Context context) {
        this.context = context;

        SharedPreferences subscribe = context.getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        firebase_enabled = subscribe.getInt("firebase_enabled", 0);


        mData = new LinkedList<>();

        if (firebase_enabled == 1) {

            firebase_loaded = 0;
            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            DatabaseReference ref1 = database1.getReference("Lists/exp_drag_drop");

            ref1.addChildEventListener(new ChildEventListener() {
                int count = 0;

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    final ListDataGroup listData = dataSnapshot.getValue(ListDataGroup.class);


                    final ConcreteGroupData group = new ConcreteGroupData(count, listData.item_title, listData.item_subtitle, listData.avatar_image_name, listData.avatar_image_url);
                    final List<ChildData> children = new ArrayList<>();

                    long childId = group.generateNewChildId();

                    children.add(new ConcreteChildData(childId, listData.subitem_title, listData.subitem_body_text, listData.rating_image_name, listData.rating_image_url, listData.rating, listData.location_image_name, listData.location_image_url, listData.location));
                    mData.add(new Pair<GroupData, List<ChildData>>(group, children));
                    count++;


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        firebase_loaded = 1;


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        } else {

            ConcreteGroupData group = new ConcreteGroupData(0, "Bates Dunn", "@bates4a4", R.drawable.ic_avatarcaptain);

            List<ChildData> children = new ArrayList<>();


            long childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Roident est duis duis sit occaecat ea eiusmod laboris mollit ullamco mollit nisi veniam.c",
                    R.drawable.ic_star, "6.3", R.drawable.ic_location_white, "Brogan, Chad"));

            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(1, "Benton Willis", "@benton", R.drawable.ic_avatarshowman);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Aboris exercitation veniam laboris incididunt duis aliquip.",
                    R.drawable.ic_star, "9.3", R.drawable.ic_location_white, "Rehrersburg, Romania"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(2, "Barbara Bernard", "@barbara333", R.drawable.ic_avatardisc_jockey);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Llamco nisi sunt velit quis sint anim nisi sunt Lorem in.",
                    R.drawable.ic_star, "8.6", R.drawable.ic_location_white, "Durham, Mauritania"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(3, "Megan Singleton", "@meganm_e", R.drawable.ic_avatarastronaut);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Ulpa aliqua sint incididunt consectetur deserunt excepteur voluptate non excepteur et esse labore.",
                    R.drawable.ic_star, "9.2", R.drawable.ic_location_white, "Callaghan, Tonga"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(4, "Guy Mccoy", "@guy", R.drawable.ic_avatardetective);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Labore dolor in sint.",
                    R.drawable.ic_star, "7.6", R.drawable.ic_location_white, "Manitou, Norway"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(5, "Cline Lindsay", "@clinexxy", R.drawable.ic_avatardisc_jockey);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "St quis consequat laboris pariatur magna.",
                    R.drawable.ic_star, "8.6", R.drawable.ic_location_white, "Weedville, Mariana Islands"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(6, "Juliette Medina", "@juliette", R.drawable.ic_avatardiver);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Iusmod enim sunt adipisicing ex enim id reprehenderit eiusmod quis non ut.",
                    R.drawable.ic_star, "5.2", R.drawable.ic_location_white, "Curtice, Nauru"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(7, "Susanna Simmons", "@susanna", R.drawable.ic_avatardetective);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Cupidatat voluptate do qui pariatur.",
                    R.drawable.ic_star, "7.6", R.drawable.ic_location_white, "Barronett, Iran"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(8, "Mildred Clark", "@mildred789", R.drawable.ic_avatardiver);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Met do mollit quis ex nostrud.",
                    R.drawable.ic_star, "9.3", R.drawable.ic_location_white, "Urie, Swaziland"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

            group = new ConcreteGroupData(9, "Valdez Bruce", "@valdez", R.drawable.ic_avatarshowman);
            children = new ArrayList<>();
            childId = group.generateNewChildId();
            children.add(new ConcreteChildData(childId, "About", "Tempor do minim eiusmod aliquip tempor reprehenderit aliquip nulla consectetur qui.",
                    R.drawable.ic_star, "8.8", R.drawable.ic_location_white, "Blackgum, Uruguay"));
            mData.add(new Pair<GroupData, List<ChildData>>(group, children));

        }
    }

    @Override
    public Integer getFirebase_enabled() {
        return firebase_enabled;
    }

    @Override
    public Integer getFirebase_loaded() {

        return firebase_loaded;
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mData.get(groupPosition).second.size();
    }

    @Override
    public GroupData getGroupItem(int groupPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            throw new IndexOutOfBoundsException("groupPosition = " + groupPosition);
        }

        return mData.get(groupPosition).first;
    }

    @Override
    public ChildData getChildItem(int groupPosition, int childPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            throw new IndexOutOfBoundsException("groupPosition = " + groupPosition);
        }

        final List<ChildData> children = mData.get(groupPosition).second;

        if (childPosition < 0 || childPosition >= children.size()) {
            throw new IndexOutOfBoundsException("childPosition = " + childPosition);
        }

        return children.get(childPosition);
    }

    @Override
    public void moveGroupItem(int fromGroupPosition, int toGroupPosition) {
        if (fromGroupPosition == toGroupPosition) {
            return;
        }

        final Pair<GroupData, List<ChildData>> item = mData.remove(fromGroupPosition);
        mData.add(toGroupPosition, item);
    }

    @Override
    public void moveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition) {
        if ((fromGroupPosition == toGroupPosition) && (fromChildPosition == toChildPosition)) {
            return;
        }

        final Pair<GroupData, List<ChildData>> fromGroup = mData.get(fromGroupPosition);
        final Pair<GroupData, List<ChildData>> toGroup = mData.get(toGroupPosition);

        final ConcreteChildData item = (ConcreteChildData) fromGroup.second.remove(fromChildPosition);

        if (toGroupPosition != fromGroupPosition) {
            // assign a new ID
            final long newId = ((ConcreteGroupData) toGroup.first).generateNewChildId();
            item.setChildId(newId);
        }

        toGroup.second.add(toChildPosition, item);
    }

    @Override
    public void removeGroupItem(int groupPosition) {
        mLastRemovedGroup = mData.remove(groupPosition);
        mLastRemovedGroupPosition = groupPosition;

        mLastRemovedChild = null;
        mLastRemovedChildParentGroupId = -1;
        mLastRemovedChildPosition = -1;
    }

    @Override
    public void removeChildItem(int groupPosition, int childPosition) {
        mLastRemovedChild = mData.get(groupPosition).second.remove(childPosition);
        mLastRemovedChildParentGroupId = mData.get(groupPosition).first.getGroupId();
        mLastRemovedChildPosition = childPosition;

        mLastRemovedGroup = null;
        mLastRemovedGroupPosition = -1;
    }


    @Override
    public long undoLastRemoval() {
        if (mLastRemovedGroup != null) {
            return undoGroupRemoval();
        } else if (mLastRemovedChild != null) {
            return undoChildRemoval();
        } else {
            return RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION;
        }
    }

    private long undoGroupRemoval() {
        int insertedPosition;
        if (mLastRemovedGroupPosition >= 0 && mLastRemovedGroupPosition < mData.size()) {
            insertedPosition = mLastRemovedGroupPosition;
        } else {
            insertedPosition = mData.size();
        }

        mData.add(insertedPosition, mLastRemovedGroup);

        mLastRemovedGroup = null;
        mLastRemovedGroupPosition = -1;

        return RecyclerViewExpandableItemManager.getPackedPositionForGroup(insertedPosition);
    }

    private long undoChildRemoval() {
        Pair<GroupData, List<ChildData>> group = null;
        int groupPosition = -1;

        // find the group
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).first.getGroupId() == mLastRemovedChildParentGroupId) {
                group = mData.get(i);
                groupPosition = i;
                break;
            }
        }

        if (group == null) {
            return RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION;
        }

        int insertedPosition;
        if (mLastRemovedChildPosition >= 0 && mLastRemovedChildPosition < group.second.size()) {
            insertedPosition = mLastRemovedChildPosition;
        } else {
            insertedPosition = group.second.size();
        }

        group.second.add(insertedPosition, mLastRemovedChild);

        mLastRemovedChildParentGroupId = -1;
        mLastRemovedChildPosition = -1;
        mLastRemovedChild = null;

        return RecyclerViewExpandableItemManager.getPackedPositionForChild(groupPosition, insertedPosition);
    }

    public static final class ConcreteGroupData extends GroupData {

        private final long mId;
        private final String mName;
        private final String mAbout;
        private final Integer mAvatarid;
        private final String mAvatar_image_name;
        private final String mAvatar_image_url;
        private boolean mPinned;
        private long mNextChildId;

        ConcreteGroupData(long id, String name, String about, Integer avatarid) {
            mId = id;
            mName = name;
            mAbout = about;
            mAvatarid = avatarid;
            mNextChildId = 0;
            mAvatar_image_name = null;
            mAvatar_image_url = null;
        }

        ConcreteGroupData(long id, String item_title, String item_subtitle, String avatar_image_name, String avatar_image_url) {
            mId = id;
            mName = item_title;
            mAbout = item_subtitle;
            mAvatar_image_name = avatar_image_name;
            mAvatar_image_url = avatar_image_url;
            mAvatarid = null;
            mNextChildId = 0;
        }


        @Override
        public long getGroupId() {
            return mId;
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public String getAvatar_image_url() {
            return mAvatar_image_url;
        }

        @Override
        public Integer getAvatarImg() {
            return mAvatarid;
        }

        @Override
        public String getAvatarImgName() {
            return mAvatar_image_name;
        }

        @Override
        public String getText() {
            return mName;
        }

        @Override
        public String getAbout() {
            return mAbout;
        }

        @Override
        public String getData1Text() {
            return null;
        }

        @Override
        public String getLocation_image_url() {
            return null;
        }

        @Override
        public String getRating_image_url() {
            return null;
        }

        @Override
        public String getData2Text() {
            return null;
        }

        @Override
        public Integer getData1Img() {
            return null;
        }

        @Override
        public Integer getData2Img() {
            return null;
        }

        @Override
        public String getRatingImgName() {
            return null;
        }

        @Override
        public String getLocationImgName() {
            return null;
        }


        @Override
        public void setPinned(boolean pinnedToSwipeLeft) {
            mPinned = pinnedToSwipeLeft;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        public long generateNewChildId() {
            final long id = mNextChildId;
            mNextChildId += 1;
            return id;
        }
    }

    public static final class ConcreteChildData extends ChildData {

        private long mId;
        private final String mTitle;
        private final String mAboutExtended;
        private final String mData1;
        private final String mData2;
        private final Integer mPicResource1;
        private final Integer mPicResource2;

        private final String mLocation_image_name;
        private final String mLocation_image_url;
        private final String mRating_image_name;
        private final String mRating_image_url;


        private boolean mPinned;

        ConcreteChildData(long id, String title, String aboutExtended, int picResource1, String data1, int picResource2, String data2) {
            mId = id;
            mTitle = title;
            mAboutExtended = aboutExtended;
            mData1 = data1;
            mData2 = data2;
            mPicResource1 = picResource1;
            mPicResource2 = picResource2;
            mLocation_image_name = null;
            mRating_image_name = null;
            mLocation_image_url = null;
            mRating_image_url = null;

        }

        ConcreteChildData(long id, String title, String aboutExtended, String rating_image_name, String rating_image_url, String data1, String location_image_name, String location_image_url, String data2) {
            mId = id;
            mTitle = title;
            mAboutExtended = aboutExtended;
            mData1 = data1;
            mData2 = data2;
            mPicResource1 = null;
            mPicResource2 = null;
            mLocation_image_name = location_image_name;
            mLocation_image_url = location_image_url;
            mRating_image_name = rating_image_name;
            mRating_image_url = rating_image_url;
        }

        @Override
        public long getChildId() {
            return mId;
        }

        @Override
        public Integer getAvatarImg() {
            return null;
        }

        @Override
        public String getAvatarImgName() {
            return null;
        }

        @Override
        public String getAvatar_image_url() {
            return null;
        }

        @Override
        public String getLocation_image_url() {
            return mLocation_image_url;
        }

        @Override
        public String getRating_image_url() {
            return mRating_image_url;
        }

        @Override
        public String getText() {
            return mTitle;
        }

        @Override
        public String getAbout() {
            return mAboutExtended;
        }

        @Override
        public String getData1Text() {
            return mData1;
        }

        @Override
        public String getData2Text() {
            return mData2;
        }

        @Override
        public Integer getData1Img() {
            return mPicResource1;
        }

        @Override
        public Integer getData2Img() {
            return mPicResource2;
        }

        @Override
        public String getRatingImgName() {
            return mRating_image_name;
        }

        @Override
        public String getLocationImgName() {
            return mLocation_image_name;
        }


        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        public void setChildId(long id) {
            this.mId = id;
        }
    }

    public static final class ListDataGroup {


        String avatar_image_name, avatar_image_url;

        String item_title;
        String span;
        String item_subtitle;
        String subitem_title;
        String subitem_body_text;
        String rating_image_name, rating_image_url;
        String rating;
        String location_image_name, location_image_url;
        String location;


        ListDataGroup() {
        }

        ListDataGroup(String avatar_image_name, String avatar_image_url, String item_title, String span, String item_subtitle, String subitem_title,
                      String subitem_body_text, String rating_image_name, String rating_image_url, String rating,
                      String location_image_name, String location_image_url, String location) {
            this.avatar_image_name = avatar_image_name;
            this.avatar_image_url = avatar_image_url;
            this.item_title = item_title;
            this.span = span;
            this.item_subtitle = item_subtitle;
            this.subitem_title = subitem_title;
            this.subitem_body_text = subitem_body_text;
            this.rating_image_name = rating_image_name;
            this.rating_image_url = rating_image_url;
            this.rating = rating;
            this.location_image_name = location_image_name;
            this.location_image_url = location_image_url;
            this.location = location;


        }

        public String getItem_subtitle() {
            return item_subtitle;
        }

        public void setItem_subtitle(String item_subtitle) {
            this.item_subtitle = item_subtitle;
        }

        public String getSpan() {

            return span;
        }

        public void setSpan(String span) {
            this.span = span;
        }

        public String getAvatar_image_name() {
            return avatar_image_name;
        }

        public void setAvatar_image_name(String avatar_image_name) {
            this.avatar_image_name = avatar_image_name;
        }

        public String getAvatar_image_url() {
            return avatar_image_url;
        }

        public String getRating_image_url() {
            return rating_image_url;
        }

        public void setRating_image_url(String rating_image_url) {
            this.rating_image_url = rating_image_url;
        }

        public String getRating_image_name() {
            return rating_image_name;
        }

        public void setRating_image_name(String rating_image_name) {
            this.rating_image_name = rating_image_name;
        }

        public String getLocation_image_name() {
            return location_image_name;
        }

        public void setLocation_image_name(String location_image_name) {
            this.location_image_name = location_image_name;
        }

        public void setAvatar_image_url(String avatar_image_url) {
            this.avatar_image_url = avatar_image_url;
        }

        public String getLocation_image_url() {
            return location_image_url;
        }

        public void setLocation_image_url(String location_image_url) {
            this.location_image_url = location_image_url;
        }

        public String getItem_title() {
            return item_title;
        }

        public void setItem_title(String item_title) {
            this.item_title = item_title;
        }
    }


}
