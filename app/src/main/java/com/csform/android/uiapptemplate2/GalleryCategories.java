package com.csform.android.uiapptemplate2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.adapter.GalleryCategoriesAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;


public class GalleryCategories extends BaseActivity {

    public static class PictureWithCategory {
        public String image_name;
        public String image_url;
        public String category;

        public PictureWithCategory() {
        }

        public PictureWithCategory(String name, String category, String image_url) {
            this.image_name = name;
            this.category = category;
            this.image_url = image_url;
        }

        public String getImage_name() {
            return image_name;
        }

        public String getCategory() {
            return category;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }
    }


    public static final String EXTRA_MESSAGE = "Gallery Category";
    final ArrayList<PictureWithCategory> listPicturesWithCategories = new ArrayList<>();
    int count = 0;
    List<String> listCategories = new ArrayList<String>();
    List<String> listImagesUrls = new ArrayList<String>();
    List<Integer> listNumPhotos = new ArrayList<Integer>();
    GalleryCategoriesAdapter adapter;
    GridView gridView;
    private StorageReference mStorageRef;
    ArrayList<ArrayList<String>> myLists = new ArrayList<>();
    Integer ex = 0;


    String[] categoryName = {
            "ANIMALS",
            "MOUNTAINS",
            "CITIES",
            "WATER",
            "MONKEY",
            "EYES",
            "RIVERS",
            "LAUGH"
    };
    int[] category_images =
            {
                    R.drawable.abstract_picture,
                    R.drawable.bird,
                    R.drawable.aiplane,
                    R.drawable.abstract_red,
                    R.drawable.branch_hand,
                    R.drawable.bridge,
                    R.drawable.building,
                    R.drawable.cave,
                    R.drawable.fireworks,
                    R.drawable.fruits,
                    R.drawable.jellyfish,
                    R.drawable.landscape,
                    R.drawable.man_sea,
                    R.drawable.man_space,
                    R.drawable.minimal_building,
                    R.drawable.mountains,
                    R.drawable.music_concert,
                    R.drawable.music_dj,
                    R.drawable.nature,
                    R.drawable.orange_jellyfish,
                    R.drawable.our_of_space,
                    R.drawable.photographer

            };

    String[] imgUrl = {

            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Fabstract-red.jpg?alt=media&token=60dcf588-5ad2-4fe2-99b9-23b6a95bd190",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Fabstract.jpg?alt=media&token=cfe7eda3-4cae-4f47-aae7-822e27928bf8",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Faiplane.jpg?alt=media&token=9fed3a1d-4034-4fb9-b3b1-bd57bdd46021",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Fbird.jpg?alt=media&token=421bcc66-3552-4088-b839-478b464d5c37",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Fbranch-hand.jpg?alt=media&token=87a8d586-fb27-47d9-95b6-0bb4d72f9d47",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Fbridge.jpg?alt=media&token=b5b440df-83a6-467d-8b94-9f288497c55f",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Fbuilding.jpg?alt=media&token=4060688f-8059-4a44-b26b-524ad961baf6",
            "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery_default%2Ffireworks.jpg?alt=media&token=4288ab53-aefa-42f0-8793-68c83dbf9020"
    };

    Integer[] numPhotos = {
            21,
            21,
            21,
            21,
            21,
            21,
            21,
            21
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        contentFrameLayout.removeAllViews();
        getLayoutInflater().inflate(R.layout.gallery_categories, contentFrameLayout);
        gridView = (GridView) contentFrameLayout.findViewById(R.id.gridViewSelect3);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.gallery_categories_title);


        SharedPreferences subscribe = getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);


        if (firebaseStatus == 1) {
            System.out.println("firebase active");
            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();
            DatabaseReference ref1 = database1.getReference("Gallery");

            ref1.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    PictureWithCategory newPost = dataSnapshot.getValue(PictureWithCategory.class);
                    listPicturesWithCategories.add(newPost);
                    setCategoriesData(count);
                    count++;
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        System.out.println("We're done loading the initial " + dataSnapshot.getChildrenCount() + " items");
                                                        getFirebasePopulatedData();
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        } else {

            adapter = new GalleryCategoriesAdapter(this, categoryName, category_images, numPhotos);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    selectCategory(position);

                                                }
                                            }
            );
        }


    }

    private void selectCategory(int position) {

        Intent intent = new Intent(this, GalleryCategory.class);
        String message = categoryName[position];

        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    private void setCategoriesData(int current) {
        ex = 0;
        if (listCategories.isEmpty()) {
            listCategories.add(listPicturesWithCategories.get(current).getCategory());
            listNumPhotos.add(1);
            listImagesUrls.add("https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");
            myLists.add(new ArrayList<String>());
            myLists.get(0).add("https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");

            if (listPicturesWithCategories.get(current).getImage_url() == null || listPicturesWithCategories.get(current).getImage_url().equals("")) {
                mStorageRef.child("gallery/" + listPicturesWithCategories.get(current).getCategory() + "/" + listPicturesWithCategories.get(current).getImage_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String uristring = uri.toString();
                        listImagesUrls.set(0, uristring);
                        myLists.get(0).set(0, uristring);
                        imgUrl = new String[listImagesUrls.size()];
                        listImagesUrls.toArray(imgUrl);

                        adapter.updateData(imgUrl);
                        adapter.notifyDataSetChanged();
                      /*  for(int i=0; i<myLists.size(); i++) {

                            for (String object : myLists.get(i)) {

                            }

                        }*/
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {


                        myLists.get(0).add("https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");
                        listImagesUrls.set(0, "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");
                    }
                });
            } else {
                String uristring = listPicturesWithCategories.get(current).getImage_url();
                listImagesUrls.set(0, uristring);
                myLists.get(0).set(0, uristring);
                imgUrl = new String[listImagesUrls.size()];
                listImagesUrls.toArray(imgUrl);

               // adapter.updateData(imgUrl);
               // adapter.notifyDataSetChanged();


            }
        } else {
            for (int i = 0; i < listCategories.size(); i++) {
                final int counter = i;
                String search = listPicturesWithCategories.get(current).getCategory();
                if (search.equals(listCategories.get(i))) {
                    ex = 1;
                    int numphotos = listNumPhotos.get(i);
                    numphotos++;
                    listNumPhotos.set(i, numphotos);
                    if (listPicturesWithCategories.get(current).getImage_url() == null || listPicturesWithCategories.get(current).getImage_url().equals("")) {
                        mStorageRef.child("gallery/" + listPicturesWithCategories.get(current).getCategory() + "/" + listPicturesWithCategories.get(current).getImage_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String uristring = uri.toString();

                                myLists.get(counter).add(uristring);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {


                            }
                        });
                        return;
                    } else {
                        String uristring = listPicturesWithCategories.get(current).getImage_url();

                        myLists.get(counter).add(uristring);
                    }

                }

            }
            if (ex == 0) {
                listCategories.add(listPicturesWithCategories.get(current).getCategory());
                listNumPhotos.add(1);
                listImagesUrls.add("https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");
                final int position = myLists.size();
                myLists.add(new ArrayList<String>());
                if (listPicturesWithCategories.get(current).getImage_url() == null || listPicturesWithCategories.get(current).getImage_url().equals("")) {
                    mStorageRef.child("gallery/" + listPicturesWithCategories.get(current).getCategory() + "/" + listPicturesWithCategories.get(current).getImage_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {

                            String uristring = uri.toString();
                            listImagesUrls.set(position, uristring);
                            myLists.get(position).add(uristring);
                            imgUrl = new String[listImagesUrls.size()];
                            listImagesUrls.toArray(imgUrl);
                            adapter.updateData(imgUrl);
                            adapter.notifyDataSetChanged();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {


                            myLists.get(0).add("https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");
                            listImagesUrls.set(0, "https://firebasestorage.googleapis.com/v0/b/android-ui-template2.appspot.com/o/gallery%2Fdefault%20(1).jpg?alt=media&token=69305177-6837-4024-8d66-ea03c30e3987");

                        }
                    });

                } else {
                    System.out.println("stigli smo ovdje");

                    String uristring = listPicturesWithCategories.get(current).getImage_url();
                    System.out.println("uristring " + uristring);
                    listImagesUrls.set(position, uristring);
                    myLists.get(position).add(uristring);
                    imgUrl = new String[listImagesUrls.size()];
                    listImagesUrls.toArray(imgUrl);
                    System.out.println(imgUrl.length);
                    // adapter.updateData(imgUrl);
                    //adapter.notifyDataSetChanged();

                }

            }

        }
    }

    private void getFirebasePopulatedData() {
        categoryName = new String[listCategories.size()];
        listCategories.toArray(categoryName);
        imgUrl = new String[listImagesUrls.size()];
        listImagesUrls.toArray(imgUrl);
        numPhotos = new Integer[listNumPhotos.size()];
        listNumPhotos.toArray(numPhotos);
        adapter = new GalleryCategoriesAdapter(this, categoryName, imgUrl, numPhotos);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                selectCategory(position);


                                            }
                                        }

        );


    }


}





