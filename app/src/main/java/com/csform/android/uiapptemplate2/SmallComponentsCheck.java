package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;



public class SmallComponentsCheck extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.small_components_check, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.check_box_title);

    }
}
