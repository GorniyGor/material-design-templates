
package android.databinding;
import com.csform.android.uiapptemplate2.BR;
class DataBinderMapperImpl extends android.databinding.DataBinderMapper {
    public DataBinderMapperImpl() {
    }
    @Override
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case com.csform.android.uiapptemplate2.R.layout.wizzard_intro_3:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/wizzard_intro_3_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.WizzardIntro3BindingImpl(bindingComponent, view);
                    }
                    if ("layout-land/wizzard_intro_3_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.WizzardIntro3BindingLandImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for wizzard_intro_3 is invalid. Received: " + tag);
                }
                case com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout-land/wizzard_intro_1_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.WizzardIntro1BindingLandImpl(bindingComponent, view);
                    }
                    if ("layout/wizzard_intro_1_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.WizzardIntro1BindingImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for wizzard_intro_1 is invalid. Received: " + tag);
                }
                case com.csform.android.uiapptemplate2.R.layout.wizards_3:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout-land/wizards_3_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.Wizards3BindingLandImpl(bindingComponent, view);
                    }
                    if ("layout/wizards_3_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.Wizards3BindingImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for wizards_3 is invalid. Received: " + tag);
                }
                case com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/wizzard_intro_2_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.WizzardIntro2BindingImpl(bindingComponent, view);
                    }
                    if ("layout-land/wizzard_intro_2_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.WizzardIntro2BindingLandImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for wizzard_intro_2 is invalid. Received: " + tag);
                }
                case com.csform.android.uiapptemplate2.R.layout.wizards_1:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/wizards_1_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.Wizards1BindingImpl(bindingComponent, view);
                    }
                    if ("layout-land/wizards_1_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.Wizards1BindingLandImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for wizards_1 is invalid. Received: " + tag);
                }
                case com.heinrichreimersoftware.materialintro.R.layout.mi_fragment_simple_slide:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/mi_fragment_simple_slide_0".equals(tag)) {
                            return new com.heinrichreimersoftware.materialintro.databinding.MiFragmentSimpleSlideBindingImpl(bindingComponent, view);
                    }
                    if ("layout-land/mi_fragment_simple_slide_0".equals(tag)) {
                            return new com.heinrichreimersoftware.materialintro.databinding.MiFragmentSimpleSlideBindingLandImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for mi_fragment_simple_slide is invalid. Received: " + tag);
                }
                case com.csform.android.uiapptemplate2.R.layout.wizards_2:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout-land/wizards_2_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.Wizards2BindingLandImpl(bindingComponent, view);
                    }
                    if ("layout/wizards_2_0".equals(tag)) {
                            return new com.csform.android.uiapptemplate2.databinding.Wizards2BindingImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for wizards_2 is invalid. Received: " + tag);
                }
                case com.heinrichreimersoftware.materialintro.R.layout.mi_fragment_simple_slide_scrollable:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/mi_fragment_simple_slide_scrollable_0".equals(tag)) {
                            return new com.heinrichreimersoftware.materialintro.databinding.MiFragmentSimpleSlideScrollableBindingImpl(bindingComponent, view);
                    }
                    if ("layout-land/mi_fragment_simple_slide_scrollable_0".equals(tag)) {
                            return new com.heinrichreimersoftware.materialintro.databinding.MiFragmentSimpleSlideScrollableBindingLandImpl(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for mi_fragment_simple_slide_scrollable is invalid. Received: " + tag);
                }
                case com.heinrichreimersoftware.materialintro.R.layout.mi_activity_intro:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/mi_activity_intro_0".equals(tag)) {
                            return new com.heinrichreimersoftware.materialintro.databinding.MiActivityIntroBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for mi_activity_intro is invalid. Received: " + tag);
                }
        }
        return null;
    }
    @Override
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    @Override
    public int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case -381032466: {
                if(tag.equals("layout/wizzard_intro_3_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizzard_intro_3;
                }
                break;
            }
            case -40913294: {
                if(tag.equals("layout-land/wizzard_intro_3_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizzard_intro_3;
                }
                break;
            }
            case -40915216: {
                if(tag.equals("layout-land/wizzard_intro_1_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1;
                }
                break;
            }
            case -381034388: {
                if(tag.equals("layout/wizzard_intro_1_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1;
                }
                break;
            }
            case 1823725804: {
                if(tag.equals("layout-land/wizards_3_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizards_3;
                }
                break;
            }
            case 1985992552: {
                if(tag.equals("layout/wizards_3_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizards_3;
                }
                break;
            }
            case -381033427: {
                if(tag.equals("layout/wizzard_intro_2_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2;
                }
                break;
            }
            case -40914255: {
                if(tag.equals("layout-land/wizzard_intro_2_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2;
                }
                break;
            }
            case 1985990630: {
                if(tag.equals("layout/wizards_1_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizards_1;
                }
                break;
            }
            case 1823723882: {
                if(tag.equals("layout-land/wizards_1_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizards_1;
                }
                break;
            }
            case 25343430: {
                if(tag.equals("layout/mi_fragment_simple_slide_0")) {
                    return com.heinrichreimersoftware.materialintro.R.layout.mi_fragment_simple_slide;
                }
                break;
            }
            case -104662078: {
                if(tag.equals("layout-land/mi_fragment_simple_slide_0")) {
                    return com.heinrichreimersoftware.materialintro.R.layout.mi_fragment_simple_slide;
                }
                break;
            }
            case 1823724843: {
                if(tag.equals("layout-land/wizards_2_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizards_2;
                }
                break;
            }
            case 1985991591: {
                if(tag.equals("layout/wizards_2_0")) {
                    return com.csform.android.uiapptemplate2.R.layout.wizards_2;
                }
                break;
            }
            case 1612553282: {
                if(tag.equals("layout/mi_fragment_simple_slide_scrollable_0")) {
                    return com.heinrichreimersoftware.materialintro.R.layout.mi_fragment_simple_slide_scrollable;
                }
                break;
            }
            case 877592774: {
                if(tag.equals("layout-land/mi_fragment_simple_slide_scrollable_0")) {
                    return com.heinrichreimersoftware.materialintro.R.layout.mi_fragment_simple_slide_scrollable;
                }
                break;
            }
            case -960528437: {
                if(tag.equals("layout/mi_activity_intro_0")) {
                    return com.heinrichreimersoftware.materialintro.R.layout.mi_activity_intro;
                }
                break;
            }
        }
        return 0;
    }
    @Override
    public String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"};
    }
}