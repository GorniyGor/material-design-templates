package com.csform.android.uiapptemplate2.databinding;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
public abstract class WizzardIntro2Binding extends ViewDataBinding {
    @NonNull
    public final android.widget.TextView miDescription;
    @NonNull
    public final android.widget.ImageView miImage;
    @NonNull
    public final android.widget.TextView miTitle;
    // variables
    protected WizzardIntro2Binding(@Nullable android.databinding.DataBindingComponent bindingComponent, @Nullable android.view.View root_, int localFieldCount
        , android.widget.TextView miDescription1
        , android.widget.ImageView miImage1
        , android.widget.TextView miTitle1
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.miDescription = miDescription1;
        this.miImage = miImage1;
        this.miTitle = miTitle1;
    }
    //getters and abstract setters
    @NonNull
    public static WizzardIntro2Binding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static WizzardIntro2Binding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static WizzardIntro2Binding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static WizzardIntro2Binding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return DataBindingUtil.<WizzardIntro2Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static WizzardIntro2Binding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return DataBindingUtil.<WizzardIntro2Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2, null, false, bindingComponent);
    }
    @NonNull
    public static WizzardIntro2Binding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return (WizzardIntro2Binding)bind(bindingComponent, view, com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2);
    }
}