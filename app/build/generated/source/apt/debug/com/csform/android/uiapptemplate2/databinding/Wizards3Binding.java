package com.csform.android.uiapptemplate2.databinding;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
public abstract class Wizards3Binding extends ViewDataBinding {
    @NonNull
    public final android.widget.TextView miDescription;
    @NonNull
    public final android.widget.ImageView miImage;
    @NonNull
    public final android.widget.TextView miTitle;
    // variables
    protected Wizards3Binding(@Nullable android.databinding.DataBindingComponent bindingComponent, @Nullable android.view.View root_, int localFieldCount
        , android.widget.TextView miDescription1
        , android.widget.ImageView miImage1
        , android.widget.TextView miTitle1
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.miDescription = miDescription1;
        this.miImage = miImage1;
        this.miTitle = miTitle1;
    }
    //getters and abstract setters
    @NonNull
    public static Wizards3Binding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static Wizards3Binding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static Wizards3Binding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static Wizards3Binding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return DataBindingUtil.<Wizards3Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizards_3, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static Wizards3Binding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return DataBindingUtil.<Wizards3Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizards_3, null, false, bindingComponent);
    }
    @NonNull
    public static Wizards3Binding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return (Wizards3Binding)bind(bindingComponent, view, com.csform.android.uiapptemplate2.R.layout.wizards_3);
    }
}